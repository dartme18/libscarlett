#pragma once

#include <spdlog/spdlog.h>

void init_logging(void);
std::string prepare_file_name(char const *);
void set_thread_name(std::string const &);
std::string get_thread_string(void);

struct logger
{
    virtual ~logger(void) {}
    virtual std::shared_ptr<spdlog::logger> get_logger(void) const;
    virtual unsigned get_uptime(void) const;

protected:
    std::chrono::high_resolution_clock::time_point start {std::chrono::high_resolution_clock::now()};
    std::shared_ptr<spdlog::logger> logger;
};
logger const & get_main_logger(void);

template <class... A>
void debug_func(std::string const & , unsigned , char const *  , A &&... )
{
    get_main_logger().get_logger()->debug("%s", "hey");
    //std::string m {"[{:06d}] " + get_thread_string() + " DEBUG " + logger + ":{:03d} -- " + message};
    //get_main_logger().get_logger()->debug(m, get_main_logger().get_uptime(), linenum, a...);
}

template <class... A>
void info_func(std::string const & , unsigned , char const * , A &&...)
{
    //std::string m {"[{:06d}] " + get_thread_string() + " INFO " + logger + ":{:03d} -- " + message};
    //get_main_logger().get_logger()->info(m, get_main_logger().get_uptime(), linenum, a...);
}

template <class... A>
void warn_func(std::string const & , unsigned , char const * , A &&...)
{
    //std::string m {"[{:06d}] " + get_thread_string() + " WARN " + logger + ":{:03d} -- " + message};
    //get_main_logger().get_logger()->warn(m, get_main_logger().get_uptime(), linenum, a...);
}

template <class... A>
void error_func(std::string const & , unsigned , char const * , A &&...)
{
    //std::string m {"[{:06d}] " + get_thread_string() + " ERROR " + logger + ":{:03d} -- " + message};
    //get_main_logger().get_logger()->error(m, get_main_logger().get_uptime(), linenum, a...);
}

#define DEBUG(...) debug_func(prepare_file_name(__FILE__), __LINE__, __VA_ARGS__)
#define INFO(...) info_func(prepare_file_name(__FILE__), __LINE__, __VA_ARGS__)
#define WARN(...) warn_func(prepare_file_name(__FILE__), __LINE__, __VA_ARGS__)
#define ERROR(...) error_func(prepare_file_name(__FILE__), __LINE__, __VA_ARGS__)
