#include "stdafx.hpp"

#include "scarlett/exception.hpp"

namespace scarlett
{

char const * exception::what(void) const noexcept
{
    return reason.c_str();
}

} /* namespace scarlett */
