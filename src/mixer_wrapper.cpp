#include "stdafx.hpp"

#include "scarlett/mixer_wrapper.hpp"
#include "scarlett/exception.hpp"

namespace scarlett
{

unsigned no_mix {static_cast<unsigned>(-1)};

std::string const master_volume_control_name {"master"};

/* Returns true if the input corresponds to a mix. */
bool is_selection_mix(std::string const & selection)
{
    return selection.rfind("Mix ", 0) != std::string::npos;
}

mixer_wrapper::~mixer_wrapper(void)
{
    mixer.unregister_all();
    INFO("Mixer wrapper destroying.");
}

/* Given an output volume, return the output enum name. */
static std::string convert_output_volume_name(std::string const & input)
{
    auto channel_num {input[input.size() - 2]};
    std::string input_num_str {input[7]};
    unsigned channel_dest {8};
    if (std::isdigit(input[8]))
        ++channel_dest;
    auto channel_char {channel_num == '0' ? 'L' : 'R'};
    auto ret {input};
    ret.insert(channel_dest, 1, channel_char);
    auto insert_spot {ret.find_last_of('(')};
    ret.erase(insert_spot);
    ret += "Source";
    return ret;
}

unsigned get_mix_number(std::string const & name)
{
    if (name.size() < 5)
        throw exception {"Mix name " + name + " must have 5 characters to calculate the mix number."};
    auto mix_letter {name[4]};
    return static_cast<unsigned>(mix_letter - 'A');
}

void mixer_wrapper::master_volume_handler(ivolume_control const & v)
{
    decltype(master_volume_changed) handlers;
    {
        lock_t {mutey};
        handlers = master_volume_changed;
    }
    for (auto const & h : handlers)
        h(v.get_db());
}

void mixer_wrapper::output_volume_handler(ivolume_control const & out)
{
    decltype(output_volume_changed) output_volume_handlers;
    decltype(output_stereo_pair_changed) output_stereo_pair_handlers;
    auto const & name {convert_output_volume_name(out.get_name())};
    std::vector<int> pair_nums;
    {
        lock_t {mutey};
        output_volume_handlers = output_volume_changed;
        output_stereo_pair_handlers = output_stereo_pair_changed;
        for (auto const & p : output_stereo_pairs)
            if (p.element0 == name || p.element1 == name)
                pair_nums.push_back(p.num);
    }
    for (auto const & h : output_volume_handlers)
        /*This isn't the right name?*/
        h(name, out.get_db());
    for (auto num : pair_nums)
        for (auto const & h : output_stereo_pair_handlers)
            h(num, get_output_stereo_pair_volume(num), get_output_stereo_pair_pan(num));
}

void mixer_wrapper::output_changed_mixes(ienum_control const & out, unsigned)
{
    DEBUG("Output, {}, changed mixes.", out.get_name());
    auto new_mix {get_mix_number(out.get_str_selection())};
    auto const & matrix {mixer.get_matrix()};
    std::vector<std::string> new_assignments;
    for (unsigned i {0}; i < matrix.get_input_count(); ++i)
    {
        auto const & input {matrix.get_matrix_input(i)};
        auto input_name {input.get_matrix_input().get_str_selection()};
        auto const & new_vol_ctl {matrix.get_matrix_volume(i, new_mix)};
        auto in_new {new_vol_ctl.get_volume_control().get_db() != new_vol_ctl.get_volume_control().get_min_db()};
        if (!in_new || input_name == off_option)
            continue;
        new_assignments.push_back(input_name);
    }
    output_assignments[out.get_name()] = new_assignments;
}

void mixer_wrapper::output_removed_mix(ienum_control const & out, unsigned)
{
    DEBUG("Output, {}, removed from mix.", out.get_name());
    auto & assignments {output_assignments[out.get_name()]};
    assignments.clear();
    auto new_val {out.get_str_selection()};
    if (new_val == off_option)
        return;
    assignments.push_back(new_val);
}

void mixer_wrapper::output_added_mix(ienum_control const & out, unsigned)
{
    auto new_val {out.get_str_selection()};
    DEBUG("Output, {}, added to mix, {}.", out.get_name(), new_val);
    auto new_mix {get_mix_number(new_val)};
    auto const & matrix {mixer.get_matrix()};
    auto & assignments {output_assignments[out.get_name()]};
    assignments.clear();
    for (unsigned i {0}; i < matrix.get_input_count(); ++i)
    {
        auto const & input {matrix.get_matrix_input(i)};
        auto input_name {input.get_matrix_input().get_str_selection()};
        if (input_name == off_option)
            continue;
        auto const & vol {matrix.get_matrix_volume(i, new_mix).get_volume_control()};
        if (vol.get_db() == vol.get_min_db())
            continue;
        assignments.push_back(input_name);
    }
}

void mixer_wrapper::output_assignment_changed(ienum_control const & out, unsigned)
{
    auto new_val {out.get_str_selection()};
    DEBUG("Output, {}, changed to mix, {}.", out.get_name(), new_val);
    auto & assignments {output_assignments[out.get_name()]};
    assignments.clear();
    if (new_val != off_option)
        assignments.push_back(new_val);
}

void mixer_wrapper::call_handlers(std::string const & output, std::function<void(void)> const & updater)
{
    std::vector<std::string> added;
    std::vector<std::string> removed;
    std::vector<std::string> changed;
    decltype(mix_volume_changed) volume_changed;
    decltype(mix_output_changed) output_changed;
    {
        lock_t {mutey};
        auto old_assignments {output_assignments[output]};
        updater();
        auto new_assignments {output_assignments[output]};
        std::sort(old_assignments.begin(), old_assignments.end());
        std::sort(new_assignments.begin(), new_assignments.end());
        //old_assignments.erase(std::unique(old_assignments.begin(), old_assignments.end()), old_assignments.end());
        //new_assignments.erase(std::unique(new_assignments.begin(), new_assignments.end()), new_assignments.end());
        std::set_difference(new_assignments.begin(), new_assignments.end(),
            old_assignments.begin(), old_assignments.end(), std::back_inserter(added));
        std::set_difference(old_assignments.begin(), old_assignments.end(),
            new_assignments.begin(), new_assignments.end(), std::back_inserter(removed));
        std::set_intersection(old_assignments.begin(), old_assignments.end(),
            new_assignments.begin(), new_assignments.end(), std::back_inserter(changed));
        volume_changed = mix_volume_changed;
        output_changed = mix_output_changed;
    }
    for (auto const & add : added)
        for (auto const & h : output_changed)
            h(output, add, true);
    for (auto const & r : removed)
        for (auto const & h : output_changed)
            h(output, r, false);
    for (auto const & c : changed)
        for (auto const & h : volume_changed)
            h(output, c, get_mix_volume(output, c));
}

void mixer_wrapper::output_assignment_handler(ienum_control const & out, unsigned old_index)
{
    auto old_val {out.get_enum_choices()[old_index]};
    auto new_val {out.get_str_selection()};
    auto name {out.get_name()};
    if (new_val == old_val)
    {
        DEBUG("output_assignment_handler - No-op output assignment change event on output {} ({})?", name, old_val);
        return;
    }
    if (is_selection_mix(old_val))
    {
        if (is_selection_mix(new_val))
            call_handlers(name, [this, &out, old_index] { output_changed_mixes(out, old_index); });
        else
            call_handlers(name, [this, &out, old_index] { output_removed_mix(out, old_index); });
    }
    else if (is_selection_mix(new_val))
        call_handlers(name, [this, &out, old_index] { output_added_mix(out, old_index); });
    else
        call_handlers(name, [this, &out, old_index] { output_assignment_changed(out, old_index); });
}

void mixer_wrapper::matrix_input_changed(imatrix_input const & input, unsigned old_index)
{
    auto new_val {input.get_matrix_input().get_str_selection()};
    auto old_val {input.get_matrix_input().get_enum_choices()[old_index]};
    if (new_val == old_val)
        return;
    /* An Input changed. All mixes are potentially affected, so loop
     * over all the outputs (and usb outputs) that have a mix assigned
     * and adjust their recorded assignments accordingly. */
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
    {
        auto const & output {mixer.get_from_all_outputs(i)};
        if (!is_selection_mix(output.get_str_selection()))
            continue;
        call_handlers(output.get_name(), [this, &output, &input, old_index] { matrix_input_changed_impl(input, output, old_index); });
    }
}

void mixer_wrapper::matrix_input_changed_impl(imatrix_input const & input, ienum_control const & output, unsigned old_index)
{
    auto mix_num {get_mix_number(output.get_str_selection())};
    auto new_val {input.get_matrix_input().get_str_selection()};
    auto old_val {input.get_matrix_input().get_enum_choices()[old_index]};
    if (new_val != off_option)
    {
        auto & assignments {output_assignments[output.get_name()]};
        auto spot {std::find(assignments.begin(), assignments.end(), new_val)};
        if (spot == assignments.end())
        {
            auto & vol {mixer.get_matrix().get_matrix_volume(input.get_input_num(), mix_num).get_volume_control()};
            if (vol.get_db() == vol.get_min_db())
                /* We're adding an input, but its volume is zero. */
                return;
            assignments.push_back(new_val);
        }
    }
    if (old_val != off_option)
    {
        auto element {output_assignments.find(output.get_name())};
        if (element == output_assignments.end())
        {
            WARN("An input changed or turned off, but this mix isn't in the mix list?");
            return;
        }
        auto & assignments {element->second};
        auto assignment {std::find(assignments.begin(), assignments.end(), old_val)};
        if (assignment == assignments.end())
        {
            WARN("Assignment, {}, not found when attempting to remove. Continuing...", old_val);
            return;
        }
        assignments.erase(assignment);
    }
}

void mixer_wrapper::matrix_volume_changed(imatrix_volume const & matrix_vol)
{
    auto const & vol {matrix_vol.get_volume_control()};
    if (vol.get_db() == vol.get_min_db())
        /* If the volume was set to minimum value, no inputs could have been changed by this adjustment. */
        return;
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
    {
        auto const & output {mixer.get_from_all_outputs(i)};
        auto selection {output.get_str_selection()};
        if (!is_selection_mix(selection))
            continue;
        if (get_mix_number(selection) != matrix_vol.get_mix_num())
            continue;
        auto input_name {mixer.get_matrix().get_matrix_input(matrix_vol.get_input_num()).get_matrix_input().get_str_selection()};
        if (input_name == off_option)
            return;
        call_handlers(output.get_name(), [this, &output, &matrix_vol] { set_outputs_after_volume_change(output, matrix_vol); });
    };
}

/**
 * If a volume changes from minimum volume, that is enough to activate an input for a mix.
 */
void mixer_wrapper::set_outputs_after_volume_change(ienum_control const & output, imatrix_volume const & matrix_vol)
{
    auto & assignments {output_assignments[output.get_name()]};
    auto input_name {mixer.get_matrix().get_matrix_input(matrix_vol.get_input_num()).get_matrix_input().get_str_selection()};
    auto spot {std::find(assignments.begin(), assignments.end(), input_name)};
    if (spot != assignments.end())
        return;
    assignments.push_back(input_name);
}

void mixer_wrapper::set_initial_output_assignments(void)
{
    for (unsigned out_num {0}; out_num < mixer.get_all_outputs_count(); ++out_num)
    {
        auto const & output {mixer.get_from_all_outputs(out_num)};
        auto const mix_num {get_output_mix(output.get_name())};
        if (mix_num == no_mix)
        {
            if (output.get_str_selection() == off_option)
                continue;
            output_assignments[output.get_name()] = {output.get_str_selection()};
            continue;
        }
        auto const & matrix {mixer.get_matrix()};
        for (unsigned input_num {0}; input_num < matrix.get_input_count(); ++input_num)
        {
            auto const & input {matrix.get_matrix_input(input_num)};
            if (input.get_matrix_input().get_str_selection() == off_option)
                continue;
            auto const & vol {matrix.get_matrix_volume(input_num, mix_num)};
            if (vol.get_volume_control().get_db() == vol.get_volume_control().get_min_db())
                continue;
            output_assignments[output.get_name()].push_back(input.get_matrix_input().get_str_selection());
        }
    }
}

mixer_wrapper::mixer_wrapper(imixer & mix)
    : mixer {mix}
{
    set_initial_output_assignments();
    mixer.add_master_volume_listener([this](auto const & v) { master_volume_handler(v); });
    mixer.add_output_volume_listener([this](auto const & out) { output_volume_handler(out); });
    auto output_assignment_listener {[this](auto const & out, unsigned new_index) { output_assignment_handler(out, new_index); }};
    mixer.add_output_assignment_listener(output_assignment_listener);
    mixer.add_usb_output_assignment_listener(output_assignment_listener);
    mixer.get_matrix().add_input_listener([this](auto & input, unsigned old) { matrix_input_changed(input, old); });
    mixer.get_matrix().add_volume_listener([this](auto & vol) { matrix_volume_changed(vol); });
}

std::vector<std::string> mixer_wrapper::get_mix_inputs(void) const
{
    std::vector<std::string> ret;
    auto const & matrix {mixer.get_matrix()};
    auto const & input {matrix.get_matrix_input(0).get_matrix_input()};
    for (auto const & str : input.get_enum_choices())
    {
        if (str == off_option)
            continue;
        ret.push_back(str);
    }
    return ret;
}

void mixer_wrapper::set_output_volume(std::string const & output_name, int vol)
{
    for (unsigned i {0}; i < mixer.get_output_count(); ++i)
    {
        if (mixer.get_output_assignment(i).get_name() != output_name)
            continue;
        return mixer.get_output_volume(i).set_db(vol);
    }
    for (unsigned i {0}; i < mixer.get_usb_output_count(); ++i)
        if (mixer.get_usb_output(i).get_name() == output_name)
            throw exception {"Cannot set output volume on USB outputs."};
    throw exception {"Could not set output volume for output " + output_name + ". (Could not find output.)"};
}

unsigned mixer_wrapper::get_unused_mix(void) const
{
    std::vector<unsigned> used_mixes;
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
    {
        auto const & output {mixer.get_from_all_outputs(i)};
        auto selection {output.get_str_selection()};
        if (!is_selection_mix(selection))
            /* This output isn't using a mix. */
            continue;
        auto index {get_mix_number(output.get_str_selection())};
        auto spot {std::find(used_mixes.begin(), used_mixes.end(), index)};
        if (spot != used_mixes.end())
            /* Mix used on two outputs? */
            continue;
        used_mixes.push_back(index);
    }
    auto const & matrix {mixer.get_matrix()};
    for (unsigned i {0}; i < matrix.get_mix_count(); ++i)
    {
        auto spot {std::find(used_mixes.begin(), used_mixes.end(), i)};
        if (spot != used_mixes.end())
            continue;
        return i;
    }
    return no_mix;
}

unsigned mixer_wrapper::get_output_mix(std::string const & output_name) const
{
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
    {
        auto const & output {mixer.get_from_all_outputs(i)};
        if (output.get_name() != output_name)
            continue;
        auto input_name {output.get_str_selection()};
        if (!is_selection_mix(input_name))
            return no_mix;
        return get_mix_number(input_name);
    }
    return no_mix;
}

/* Ensure that the specified input is available on the matrix mixer. */
imatrix_input const & mixer_wrapper::ensure_input_available(std::string const & sought_input)
{
    auto & matrix {mixer.get_matrix()};
    /* Search for an input already assigned to the desired input. */
    for (unsigned i {0}; i < matrix.get_input_count(); ++i)
    {
        auto const & input {matrix.get_matrix_input(i)};
        if (sought_input == input.get_matrix_input().get_str_selection())
            return input;
    }
    std::vector<std::string> already_seen;
    for (unsigned i {0}; i < matrix.get_input_count(); ++i)
    {
        auto & input {matrix.get_matrix_input(i)};
        auto const temp_input_name {input.get_matrix_input().get_str_selection()};
        auto spot {std::find(already_seen.begin(), already_seen.end(), temp_input_name)};
        auto duplicate {spot != already_seen.end()};
        already_seen.push_back(temp_input_name);
        /* If the input is already present as a matrix input, we can reassign it. */
        if (!duplicate && temp_input_name != off_option)
            continue;
        input.get_matrix_input().set_selection(sought_input);
        return input;
    }
    throw exception {"Attempt to add input " + sought_input + " to matrix mixes failed; no slot available for input."};
}

unsigned get_mix_index(std::vector<std::string> const & values, unsigned mix)
{
    for (unsigned num {0}; num < values.size(); ++num)
    {
        auto const & val {values[num]};
        if (!is_selection_mix(val))
            continue;
        auto temp_mix_num {get_mix_number(val)};
        if (temp_mix_num != mix)
            continue;
        return num;
    }
    return no_mix;
}

/* Applicable to USB outputs, too! */
void mixer_wrapper::assign_mix_to_output(std::string const & output_name, unsigned mix)
{
    DEBUG("Assigning mix, {}, to output, {}.", mix, output_name);
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
    {
        auto & output {mixer.get_from_all_outputs(i)};
        if (output.get_name() != output_name)
            continue;
        output.set_selection(get_mix_index(output.get_enum_choices(), mix));
        return;
    }
    throw exception {"Unable to find output with name " + output_name + ".  " __FILE__ ":" + std::to_string(__LINE__)};
}

void mixer_wrapper::add_input_to_mix(std::string const & input_name, unsigned mix_num)
{
    DEBUG("Adding input, {}, to mix, {}.", input_name, mix_num);
    auto const & input {ensure_input_available(input_name)};
    auto & volume {mixer.get_matrix().get_matrix_volume(input.get_input_num(), mix_num).get_volume_control()};
    if (volume.get_db() == volume.get_min_db())
        volume.set_db(volume.get_db() + volume.minimum_step_size());
}

bool mixer_wrapper::add_input_to_output(std::string const & output_name, std::string const & input_name)
{
    auto current_mix {get_output_mix(output_name)};
    DEBUG("Adding input, {}, to output, {}.", input_name, output_name);
    if (input_name == off_option)
        return current_mix != no_mix;
    if (current_mix != no_mix)
    {
        /* This output is already on a mix */
        add_input_to_mix(input_name, current_mix);
        return true;
    }
    auto & output {get_output(output_name)};
    if (output.get_str_selection() == off_option)
    {
        DEBUG("Setting output to single input.");
        /* First input added to output; it's special. */
        output.set_selection(input_name);
        return false;
    }
    /* Create a mix with the current input plus the new input. */
    current_mix = get_unused_mix();
    DEBUG("Creating mix {}.", current_mix);
    if (current_mix == no_mix)
        throw exception {"Cannot create new mix for output, " + output_name + " to add input " + input_name + ". No mixes available."};
    reset_mix(current_mix);
    add_input_to_mix(output.get_str_selection(), current_mix);
    add_input_to_mix(input_name, current_mix);
    assign_mix_to_output(output_name, current_mix);
    return true;
}

/* Returns the number of inputs for the output after removing the specified input. */
unsigned mixer_wrapper::remove_input_from_output_mix(std::string const & output_name, std::string const & input_name)
{
    std::vector<std::string> assignments;
    {
        lock_t {mutey};
        assignments = output_assignments[output_name];
    }
    if (assignments.size() == 2)
    {
        auto spot {std::find_if(assignments.begin(), assignments.end(), [&input_name](auto const & a) {
            return a != input_name;
        })};
        if (spot == assignments.end())
            throw exception {"Unable to find input besides " + input_name + " for output " + output_name + " when trying to remove."};
        auto & output {get_output(output_name)};
        output.set_selection(*spot);
        return 1;
    }
    auto vol_opt {get_volume_control(output_name, input_name)};
    if (!vol_opt)
        throw exception {"Could not find matrix volume control for output, " + output_name + ", input name, " + input_name + "!"};
    auto & vol {vol_opt->get()};
    vol.set_db(vol.get_min_db());
    unsigned ret {12345678};
    DEBUG("Removing input {} from output {}.", input_name, output_name);
    call_handlers(output_name, [this, &output_name, &input_name, &ret] {
        auto & assignments = output_assignments[output_name];
        DEBUG("    Before removal, {} inputs.", assignments.size());
        auto spot {std::find(assignments.begin(), assignments.end(), input_name)};
        while (spot != assignments.end())
        {
            assignments.erase(spot);
            spot = std::find(assignments.begin(), assignments.end(), input_name);
        }
        ret = assignments.size();
        DEBUG("    Assignments is now {} long.", ret);
        for (auto const & a : assignments)
            DEBUG("        {}.", a);
    });
    DEBUG("Finished removing input {} from output {}.", input_name, output_name);
    return ret;
}

bool mixer_wrapper::remove_input_from_output(std::string const & output_name, std::string const & input_name)
{
    auto current_mix {get_output_mix(output_name)};
    auto & output {get_output(output_name)};
    if (current_mix != no_mix)
        return remove_input_from_output_mix(output_name, input_name) > 1;
    output.set_selection(off_option);
    return false;
}

std::vector<input_assignment> mixer_wrapper::get_inputs(std::string const & output_name) const
{
    auto current_mix {get_output_mix(output_name)};
    if (current_mix == no_mix)
    { /* no mix. */
        for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
        {
            auto const & output {mixer.get_from_all_outputs(i)};
            if (output.get_name() != output_name)
                continue;
            auto temp_input {output.get_str_selection()};
            if (temp_input == off_option)
                return {};
            return {{temp_input, static_cast<int>(no_volume)}};
        }
        return {};
    }
    /* A copy of the assignments. */
    std::vector<std::string> assignments;
    {
        lock_t {mutey};
        auto assignment_it {output_assignments.find(output_name)};
        if (assignment_it == output_assignments.end())
            return {};
        assignments = assignment_it->second;
    }
    std::vector<input_assignment> ret;
    std::transform(assignments.begin(), assignments.end(), std::back_inserter(ret), [this, current_mix](auto const & input) {
        auto const & matrix {mixer.get_matrix()};
        for (unsigned i {0}; i < matrix.get_input_count(); ++i)
        {
            auto const & temp_input {matrix.get_matrix_input(i)};
            if (temp_input.get_matrix_input().get_str_selection() != input)
                continue;
            auto const & temp_volume {matrix.get_matrix_volume(i, current_mix).get_volume_control()};
            return input_assignment {input, static_cast<int>(temp_volume.get_db())};
        }
        ERROR("Unable to find input, {}, in matrix! Continuing.", input);
        return input_assignment {input, no_volume};
    });
    return ret;
}

std::vector<std::string> mixer_wrapper::get_outputs(void) const
{
    std::vector<std::string> ret;
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
        ret.push_back(mixer.get_from_all_outputs(i).get_name());
    return ret;
}

int mixer_wrapper::get_output_volume(std::string const & output_name) const
{
    for (unsigned i {0}; i < mixer.get_output_count(); ++i)
    {
        auto const & output {mixer.get_output_assignment(i)};
        if (output.get_name() != output_name)
            continue;
        return static_cast<int>(mixer.get_output_volume(i).get_db());
    }
    for (unsigned i {0}; i < mixer.get_usb_output_count(); ++i)
        if (mixer.get_usb_output(i).get_name() == output_name)
            return no_volume;
    throw exception {"Unable to get output volume for output " + output_name + ". Perhaps no output exists."};
}

void mixer_wrapper::add_mix_volume_listener(mix_volume_changed_handler_t const & listener)
{
    lock_t {mutey};
    mix_volume_changed.push_back(listener);
}

void mixer_wrapper::set_mix_volume(std::string const & output_name, std::string const & input_name, int new_vol)
{
    auto vol {get_volume_control(output_name, input_name)};
    if (!vol)
        throw exception {"Cannot set volume on output, " + output_name + ", input, " + input_name + "."};
    return vol->get().set_db(new_vol);
}

int mixer_wrapper::get_master_volume(void) const
{
    return static_cast<int>(mixer.get_master_volume().get_db());
}

void mixer_wrapper::set_master_volume(int new_vol)
{
    mixer.get_master_volume().set_db(new_vol);
}

void mixer_wrapper::add_output_volume_listener(output_volume_changed_handler_t const & handler)
{
    lock_t {mutey};
    output_volume_changed.push_back(handler);
}

void mixer_wrapper::add_mix_output_listener(mix_output_changed_handler_t const & handler)
{
    lock_t {mutey};
    mix_output_changed.push_back(handler);
}

void mixer_wrapper::add_master_volume_listener(volume_changed_handler_t const & handler)
{
    lock_t {mutey};
    master_volume_changed.push_back(handler);
}

std::optional<std::reference_wrapper<ivolume_control const>> mixer_wrapper::get_volume_control(std::string const & output, std::string const & input) const
{
    auto current_mix {get_output_mix(output)};
    if (current_mix == no_mix)
        return {};
    auto & matrix {mixer.get_matrix()};
    for (unsigned input_num {0}; input_num < matrix.get_input_count(); ++input_num)
    {
        auto const & temp_input {matrix.get_matrix_input(input_num).get_matrix_input()};
        if (temp_input.get_str_selection() == input)
            return matrix.get_matrix_volume(input_num, current_mix).get_volume_control();
    }
    throw exception {"mixer_wrapper::get_volume_control: Input, " + input + ", unavailable to mixes. Call add_input_to_output first."};
}

std::optional<std::reference_wrapper<ivolume_control>> mixer_wrapper::get_volume_control(std::string const & output, std::string const & input)
{
    mixer_wrapper const * const_this {this};
    auto const_val {const_this->get_volume_control(output, input)};
    if (!const_val)
        return {};
    auto const & const_ref {const_val->get()};
    return {const_cast<ivolume_control &>(const_ref)};
}

int mixer_wrapper::get_mix_input_min_db(std::string const & output, std::string const & input) const
{
    auto vol {get_volume_control(output, input)};
    if (!vol)
        return no_volume;
    return vol->get().get_min_db();
}

int mixer_wrapper::get_mix_input_max_db(std::string const & output, std::string const & input) const
{
    auto vol {get_volume_control(output, input)};
    if (!vol)
        return no_volume;
    return vol->get().get_max_db();
}

int mixer_wrapper::get_mix_volume(std::string const & output_name, std::string const & input_name) const
{
    auto vol {get_volume_control(output_name, input_name)};
    if (!vol)
        return no_volume;
    return vol->get().get_db();
}

ienum_control & mixer_wrapper::get_output(std::string const & output_name)
{
    for (unsigned i {0}; i < mixer.get_all_outputs_count(); ++i)
    {
        auto & output {mixer.get_from_all_outputs(i)};
        if (output.get_name() == output_name)
            return output;
    }
    throw exception {"Could not find output named " + output_name + "."};
}

void mixer_wrapper::reset_mix(unsigned mix)
{
    auto & matrix {mixer.get_matrix()};
    for (unsigned i {0}; i < matrix.get_input_count(); ++i)
    {
        auto & volume {matrix.get_matrix_volume(i, mix).get_volume_control()};
        volume.set_db(volume.get_min_db());
    }
}

std::vector<stereo_pair>::const_iterator mixer_wrapper::get_output_stereo_pair(std::string const & out0, std::string const & out1) const
{
    INFO("  Finding stereo pair for outputs '{}' and '{}'.", out0, out1);
    return std::find_if(output_stereo_pairs.begin(), output_stereo_pairs.end(), [&out0, &out1](auto const & p) {
        return (p.element0 == out0 && p.element1 == out1) || (p.element0 == out1 && p.element1 == out0);
    });
}

int mixer_wrapper::create_output_stereo_pair(std::string const & out0, std::string const & out1)
{
    lock_t {mutey};
    INFO("Creating stereo pair with '{}' and '{}'. Validing we don't already have such a pair...", out0, out1);
    auto pair_it {get_output_stereo_pair(out0, out1)};
    if (pair_it != output_stereo_pairs.end())
        throw std::invalid_argument {"Pair already exists."};
    auto max_num_it {std::max_element(output_stereo_pairs.begin(), output_stereo_pairs.end(), [](auto const & lhs, auto const & rhs) {
        return lhs.num < rhs.num;
    })};
    INFO("  Finding biggest existing pair number (so we can return a bigger one...");
    int ret_num = stereo_pair::no_pair;
    if (max_num_it == output_stereo_pairs.end())
    {
        INFO("  no pairs yet? {} pairs.", output_stereo_pairs.size());
        ret_num = 0;
    }
    else
    {
        ret_num = max_num_it->num + 1;
        INFO("  Biggest num is {}. We'll use {}.", max_num_it->num, ret_num);
    }
    output_stereo_pairs.push_back({ret_num, out0, out1});
    INFO("Added output stereo pair. now {} pairs. Returning.", output_stereo_pairs.size());
    return ret_num;
}

static constexpr double pi = std::acos(-1);

static double normalize_volume(ivolume_control const & v)
{
    auto db {static_cast<double>(v.get_db())};
    auto min {v.get_min_db()};
    auto max {v.get_max_db()};
    auto ret {(db - min) / (max - min)};
    INFO("     Volume control '{}' has db {}, min {}, max {}. Normalized is {}.", v.get_name(), db, min, max, ret);
    return ret;
}

/* Returns a value in the range [0,1]. */
double mixer_wrapper::get_output_stereo_pair_raw_volume(int pair) const
{
    auto elements {get_volume_elements_for_output_pair(get_output_stereo_pair(pair))};
    double L {normalize_volume(*elements.first)};
    double R {normalize_volume(*elements.second)};
    auto pan {get_output_stereo_pair_pan_rad(pair)};
    double ret;
    if (L < 0.0001)
    {
        if (R == 0)
            return 0;
        ret = R / std::sin(pan);
    }
    else
    {
        ret = L / std::cos(pan);
    }
    INFO("  volume pair {} has Left volume {}, pan {}. Calculated normalized volume is {}.", pair, L, pan, ret);
    return ret;
}

int mixer_wrapper::get_output_stereo_pair_volume(int pair) const
{
    auto raw {get_output_stereo_pair_raw_volume(pair)};
    auto max {get_output_stereo_pair_max_db(pair)};
    auto min {get_output_stereo_pair_min_db(pair)};
    auto ret {raw * (max - min) + min};
    INFO("Volume pair {} has raw volume {}, min {}, max {}. Calculated volume is {}.", pair, raw, min, max, ret);
    return lround(ret);
}

double mixer_wrapper::get_output_stereo_pair_pan(int pair) const
{
    /* Normalize from [0,pi/2] -> [-1,1]. */
    auto raw_pan {get_output_stereo_pair_pan_rad(pair)};
    auto ranged_pan {raw_pan * 4 / pi - 1};
    return ranged_pan;
}

double mixer_wrapper::get_output_stereo_pair_pan_rad(int pair) const
{
    /* 4.5db pan law
     * L(p) = v * sqrt((pi/2 - p)* 2/pi * cos(p))
     * R(p) = v * sqrt(p* 2/pi * sin(p))
     * p is in [0, pi/2]
     * v is overall volume */
    /* Constant power pan law
     * L(p) = v * cos(p)
     * R(p) = v * sin(p)
     * p is in [0, pi/2]
     * v is overall volume.
     * v = L/cos(p)
     * p = asin(R/(L/cos(p))
     * sin(p) = R/ (L/cos(p))
     * sin(p) = Rcos(p)/L
     * p = atan(R/L) */
    auto elements {get_volume_elements_for_output_pair(get_output_stereo_pair(pair))};
    double L {normalize_volume(*elements.first)};
    double R {normalize_volume(*elements.second)};
    if (L == 0)
    {
        if (R == 0)
            /* In the middle */
            return pi / 4;
        /* Far right */
        return pi / 2;
    }
    auto pan_rad {std::atan(R / L)};
    return pan_rad;
}

void mixer_wrapper::set_output_stereo_pair_pan(int pair, double pan)
{
    auto vol {get_output_stereo_pair_raw_volume(pair)};
    auto adjusted_pan {(pan + 1) * pi / 4};
    auto L = vol * std::cos(adjusted_pan);
    auto R = vol * std::sin(adjusted_pan);
    auto elements {get_volume_elements_for_output_pair(get_output_stereo_pair(pair))};
    auto min {get_output_stereo_pair_min_db(pair)};
    auto max {get_output_stereo_pair_max_db(pair)};
    elements.first->set_db(lround(L * (max - min) + min));
    elements.second->set_db(lround(R * (max - min) + min));
}

void mixer_wrapper::set_output_stereo_pair_volume(int pair, int vol)
{
    auto pan {get_output_stereo_pair_pan_rad(pair)};
    auto min {get_output_stereo_pair_min_db(pair)};
    auto max {get_output_stereo_pair_max_db(pair)};
    auto normalized_volume {(static_cast<double>(vol) - min) / (max - min)};
    auto norm_l {normalized_volume * std::cos(pan)};
    auto norm_r {normalized_volume * std::sin(pan)};
    auto L {norm_l * (max - min) + min};
    auto R {norm_r * (max - min) + min};
    auto elements {get_volume_elements_for_output_pair(get_output_stereo_pair(pair))};
    elements.first->set_db(lround(L));
    elements.second->set_db(lround(R));
}

stereo_pair mixer_wrapper::get_output_stereo_pair(int num) const
{
    lock_t {mutey};
    INFO("Getting stereo pair number {}.", num);
    auto it {std::find_if(output_stereo_pairs.begin(), output_stereo_pairs.end(), [num](auto const & p) {
        return p.num == num;
    })};
    if (it == output_stereo_pairs.end())
        throw std::invalid_argument {"No Pair with that number exists."};
    return *it;
}

std::pair<ivolume_control *, ivolume_control *> mixer_wrapper::get_volume_elements_for_output_pair(stereo_pair const & pair)
{
    auto ret {const_cast<mixer_wrapper const *>(this)->get_volume_elements_for_output_pair(pair)};
    return {const_cast<ivolume_control *>(ret.first), const_cast<ivolume_control *>(ret.second)};
}

std::pair<ivolume_control const *, ivolume_control const *> mixer_wrapper::get_volume_elements_for_output_pair(stereo_pair const & pair) const
{
    std::pair<ivolume_control const *, ivolume_control const *> ret;
    bool got_one {false};
    bool got_two {false};
    INFO("Getting volume controls for output pair {}.", (void *)&pair);
    INFO("    Pair number {}.", pair.num);
    for (unsigned i {0}; i < mixer.get_output_count(); ++i)
    {
        auto const & output {mixer.get_output_volume(i)};
        auto const & temp_name {convert_output_volume_name(output.get_name())};
        INFO("  checking '{}'.", temp_name);
        if (temp_name == pair.element0)
        {
            got_one = true;
            ret.first = &output;
        }
        if (temp_name == pair.element1)
        {
            got_two = true;
            ret.second = &output;
        }
        if (got_one && got_two)
            break;
    }
    if (!got_one)
        throw std::invalid_argument {"Couldn't find volume element for '" + pair.element0 + "'."};
    if (!got_two)
        throw std::invalid_argument {"Couldn't find volume element for '" + pair.element1 + "'."};
    return ret;
}

int mixer_wrapper::get_output_stereo_pair_max_db(int pair_num) const
{
    lock_t {mutey};
    auto pair {get_output_stereo_pair(pair_num)};
    auto controls {get_volume_elements_for_output_pair(pair)};
    INFO("Returning max db for pair {}.", pair_num);
    return std::max(controls.first->get_max_db(), controls.second->get_max_db());
}

int mixer_wrapper::get_output_stereo_pair_min_db(int pair_num) const
{
    lock_t {mutey};
    auto pair {get_output_stereo_pair(pair_num)};
    auto controls {get_volume_elements_for_output_pair(pair)};
    INFO("Returning min db for pair {}.", pair_num);
    return std::min(controls.first->get_min_db(), controls.second->get_min_db());
}

void mixer_wrapper::add_output_stereo_pair_changed_listener(stereo_pair_changed_handler_t const & h)
{
    lock_t {mutey};
    output_stereo_pair_changed.push_back(h);
}

std::string const mixer_wrapper::off_option {"Off"};
int const mixer_wrapper::no_volume {std::numeric_limits<int>::max()};

} /* namespace scarlett */
