#include "stdafx.hpp"

#include "dummy_device.hpp"

namespace scarlett
{

#include "dummy_devices.hpp"

/* Without this mutex, it is possible that callbacks are occurring even after calling unregister_all.
 * This also means that one should not call unregister_all from an element change handler (because
 * the mutex is not recursive)! */
static std::mutex unregister_mutex;
using lock_t = std::lock_guard<std::mutex>;

idevice_ptr get_dummy_device(decltype(mock_18i8()) const & elements, std::string const & dev, bool animate)
{
    return std::make_unique<dummy_device>(std::vector<dummy_enum_ptr> {elements.first.begin(), elements.first.end()},
        std::vector<dummy_volume_ptr> {elements.second.begin(), elements.second.end()}, dev, animate);
}

idevice_ptr get_dummy_device(std::string const & device_type)
{
    decltype(mock_18i8()) elements;
    std::string animate_ending {":a"};
    bool animate {device_type.size() > 1 && std::equal(animate_ending.rbegin(), animate_ending.rend(), device_type.rbegin())};
    if (device_type.rfind("18i8", 0) != std::string::npos)
        elements = mock_18i8();
    else if (device_type.rfind("18i6", 0) != std::string::npos)
        elements = mock_18i6();
    else
        return {};
    return get_dummy_device(elements, "dummy:" + device_type, animate);
}

dummy_device::dummy_device(std::vector<dummy_enum_ptr> && enums, std::vector<dummy_volume_ptr> && volumes, std::string const & device_name, bool animate)
    : enums {std::move(enums)}
    , volumes {std::move(volumes)}
    , device_id {device_name}
{
    if (animate)
        t = std::thread {[this] { this->animate(); }};
}

void dummy_device::animate(void)
{
    unsigned const wait {20};
    while (keep_going)
    {
        unsigned count {0};
        while (++count < wait && keep_going)
            std::this_thread::sleep_for(std::chrono::milliseconds {2});
        if (!keep_going)
            break;
        for (auto const & v : volumes)
            v->change_value();
    }
}

dummy_device::~dummy_device(void)
{
    keep_going = false;
    if (t.joinable())
        t.join();
}

std::vector<ienum_ptr> dummy_device::get_enums(void) const
{
    return {enums.begin(), enums.end()};
}

std::vector<ivolume_ptr> dummy_device::get_volumes(void) const
{
    return {volumes.begin(), volumes.end()};
}

dummy_enum::dummy_enum(std::string const & name, std::vector<std::string> const & selections)
    : name {name}
    , selections {selections}
{
}

std::vector<std::string> dummy_enum::get_enum_choices(void) const
{
    return selections;
}

unsigned dummy_enum::get_selection(void) const
{
    return current_selection;
}

void dummy_enum::set_selection(unsigned val)
{
    lock_t {unregister_mutex};
    unsigned old_val;
    decltype(enum_changed) temp_handlers;
    {
        lock_t l {mutey};
        old_val = current_selection;
        current_selection = val;
        temp_handlers = enum_changed;
    }
    for (auto const & handler : temp_handlers)
        handler.second(*this, old_val);
}

std::string dummy_enum::get_name(void) const
{
    return name;
}

dummy_volume::dummy_volume(std::string const & name, long min_db, long max_db)
    : name {name}
    , min_db {min_db}
    , max_db {max_db}
    , current_db {0}
    , going_up {true}
{
}

long dummy_volume::get_max_db(void) const
{
    return max_db;
}

long dummy_volume::get_min_db(void) const
{
    return min_db;
}

void dummy_volume::set_db(long val)
{
    lock_t {unregister_mutex};
    decltype(volume_changed) temp_handlers;
    {
        lock_t l {mutey};
        current_db = val;
        temp_handlers = volume_changed;
    }
    for (auto const & h : temp_handlers)
        h.second(*this);
}

long dummy_volume::get_db(void) const
{
    lock_t l {mutey};
    return current_db;
}

void dummy_volume::register_volume_changed(void const * token, volume_handler_t const & func)
{
    lock_t l {mutey};
    volume_changed.push_back({token, func});
}

void dummy_enum::register_enum_changed(void const * token, enum_handler_t const & handler)
{
    lock_t l {mutey};
    enum_changed.push_back({token, handler});
}

std::string dummy_volume::get_name(void) const
{
    return name;
}

void dummy_volume::change_value(double severity)
{
    auto current_vol {get_db()};
    auto dist {max_db - min_db};
    auto diff {dist * severity};
    if (going_up)
    {
        auto room_up {max_db - current_vol};
        if (room_up > diff)
        {
            set_db(current_vol + diff);
            return;
        }
        set_db(max_db);
        going_up = false;
        return;
    }
    auto room_down {current_vol - min_db};
    if (room_down > diff)
    {
        set_db(current_vol - diff);
        return;
    }
    set_db(min_db);
    going_up = true;
    return;
}

long dummy_volume::minimum_step_size(void) const
{
    return 10;
}

std::string dummy_device::get_device_id(void) const
{
    return device_id;
}

void dummy_device::unregister_all(void)
{
    lock_t {unregister_mutex};
    for (auto const & e : enums)
        e->unregister_all();
    for (auto const & v : volumes)
        v->unregister_all();
}

void dummy_device::unregister_all(void const * token)
{
    lock_t {unregister_mutex};
    for (auto const & e : enums)
        e->unregister_all(token);
    for (auto const & v : volumes)
        v->unregister_all(token);
}

void dummy_volume::unregister_all(void)
{
    lock_t {mutey};
    volume_changed.clear();
}

void dummy_volume::unregister_all(void const * token)
{
    lock_t {mutey};
    for (unsigned i {0}; i < volume_changed.size(); ++i)
        if (volume_changed[i].first == token)
            volume_changed.erase(volume_changed.begin() + i--);
}

void dummy_enum::unregister_all(void)
{
    lock_t {mutey};
    enum_changed.clear();
}

void dummy_enum::unregister_all(void const * token)
{
    lock_t {mutey};
    for (unsigned i {0}; i < enum_changed.size(); ++i)
        if (enum_changed[i].first == token)
            enum_changed.erase(enum_changed.begin() + i--);
}

void dummy_device::register_device_disconnected(void const *, disconnect_handler_t const &)
{
}

bool dummy_device::is_disconnected(void) const
{
    return false;
}

} /* namespace scarlett*/
