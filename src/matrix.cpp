#include "stdafx.hpp"

#include "matrix.hpp"
#include "scarlett/exception.hpp"

namespace scarlett
{

std::optional<unsigned> get_number(std::string const & input)
{
    std::regex numbers {"([0-9]+)"};
    std::smatch matches;
    if (!std::regex_search(input, matches, numbers))
        return {};
    return std::stoi(matches[1].str());
}

std::optional<unsigned> get_mix(std::string const & input)
{
    std::regex numbers {"Mix ([A-Z]+)"};
    std::smatch matches;
    if (!std::regex_search(input, matches, numbers))
        return {};
    return {matches[1].str()[0] - 'A'};
}

matrix::~matrix(void)
{
    for (auto const & v : volume_ptrs)
        v->unregister_all(this);
    for (auto const & e : enum_ptrs)
        e->unregister_all(this);
}

void matrix::set_matrix_volumes()
{
    for (auto & temp_volume : volume_ptrs)
    {
        auto name {temp_volume->get_name()};
        auto matrix_input_num {get_number(name)};
        if (!matrix_input_num)
        {
            ERROR("There should be a number in the element name, %s. Continuing...", name);
            continue;
        }
        auto mix_num {get_mix(name)};
        if (!mix_num)
        {
            ERROR("Couldn't parse mix from element name, %s. Continuing...", name);
            continue;
        }
        auto temp_vol {std::make_shared<matrix_volume>(*matrix_input_num - 1, *mix_num, *temp_volume)};
        volumes.push_back(temp_vol);
        temp_volume->register_volume_changed(this, [this, temp_vol](auto &) {
            decltype(volume_changed) handlers;
            {
                lock_t l {mutey};
                handlers = volume_changed;
            }
            for (auto const & i : handlers)
                i(*temp_vol);
        });
    }
}

void matrix::set_inputs()
{
    for (auto & temp_enum : enum_ptrs)
    {
        auto name {temp_enum->get_name()};
        auto input_num {get_number(name)};
        if (!input_num)
        {
            ERROR("There should be a number in the element name, %s. Continuing...", name);
            continue;
        }
        /* Move to base 0 */
        auto el {std::make_shared<matrix_input>(*input_num - 1, *temp_enum)};
        inputs.push_back(el);
        temp_enum->register_enum_changed(this, [this, el](auto &, unsigned idx) {
            decltype(input_changed) handlers;
            {
                lock_t l {mutey};
                handlers = input_changed;
            }
            for (auto const & i : handlers)
                i(*el, idx);
        });
    }
}

void matrix::set_elements(std::vector<ienum_ptr> && enums, std::vector<ivolume_ptr> && volumes)
{
    if (this->volumes.size() || this->inputs.size())
        throw exception {"scarlett_matrix initialized a second time"};
    enum_ptrs = std::move(enums);
    volume_ptrs = std::move(volumes);
    set_matrix_volumes();
    set_inputs();
}

unsigned matrix::get_input_count(void) const
{
    return inputs.size();
}

unsigned matrix::get_mix_count(void) const
{
    return volumes.size() / inputs.size();
}

imatrix_volume const & matrix::get_matrix_volume(unsigned input_num, unsigned mix_num) const
{
    auto spot {std::find_if(volumes.begin(), volumes.end(), [input_num, mix_num](auto const & a) {
        return a->get_input_num() == input_num && a->get_mix_num() == mix_num;
    })};
    if (spot == volumes.end())
        throw exception {"No matrix volume at coordinate (input=" + std::to_string(input_num) + ", mix=" + std::to_string(mix_num) + ")."};
    return **spot;
}

imatrix_volume & matrix::get_matrix_volume(unsigned input_num, unsigned mix_num)
{
    return const_cast<imatrix_volume &>(const_cast<matrix const &>(*this).get_matrix_volume(input_num, mix_num));
}

imatrix_input const & matrix::get_matrix_input(unsigned input_num) const
{
    auto spot {std::find_if(inputs.begin(), inputs.end(), [input_num](auto const & a) {
        return a->get_input_num() == input_num;
    })};
    if (spot == inputs.end())
        throw exception {"No matrix input for index " + std::to_string(input_num) + "."};
    return **spot;
}

imatrix_input & matrix::get_matrix_input(unsigned input_num)
{
    auto spot {std::find_if(inputs.begin(), inputs.end(), [input_num](auto const & a) {
        return a->get_input_num() == input_num;
    })};
    if (spot == inputs.end())
        throw exception {"No matrix input for index " + std::to_string(input_num) + "."};
    return **spot;
}

ivolume_control const & matrix_volume::get_volume_control(void) const
{
    return control;
}

ivolume_control & matrix_volume::get_volume_control(void)
{
    return control;
}

unsigned matrix_volume::get_mix_num(void) const
{
    return mix;
}

unsigned matrix_volume::get_input_num(void) const
{
    return input;
}

matrix_volume::matrix_volume(unsigned input, unsigned mix, ivolume_control & volume)
    : input {input}
    , mix {mix}
    , control {volume}
{
}

matrix_input::matrix_input(unsigned input, ienum_control & control)
    : input_num {input}
    , control {control}
{
}

unsigned matrix_input::get_input_num(void) const
{
    return input_num;
}

ienum_control & matrix_input::get_matrix_input(void)
{
    return control;
}

ienum_control const & matrix_input::get_matrix_input(void) const
{
    return control;
}

std::string matrix_input::get_input_name(void) const
{
    return control.get_name();
}

void matrix::add_volume_listener(volume_handler_t const & handler)
{
    lock_t l {mutey};
    volume_changed.push_back(handler);
}

void matrix::add_input_listener(input_handler_t const & handler)
{
    lock_t l {mutey};
    input_changed.push_back(handler);
}

}
