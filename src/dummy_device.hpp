#pragma once

#include "scarlett/scarlett.hpp"

namespace scarlett
{

using dummy_volume_ptr = std::shared_ptr<struct dummy_volume>;
using dummy_enum_ptr = std::shared_ptr<struct dummy_enum>;

struct dummy_device : public scarlett::idevice
{
    dummy_device(std::vector<dummy_enum_ptr> &&, std::vector<dummy_volume_ptr> &&, std::string const & device_name, bool animate);
    ~dummy_device(void);
    std::vector<ienum_ptr> get_enums(void) const override;
    std::vector<ivolume_ptr> get_volumes(void) const override;
    std::string get_device_id(void) const override;
    void unregister_all(void const *) override;
    void unregister_all(void) override;
    void register_device_disconnected(void const * token, disconnect_handler_t const &) override;
    bool is_disconnected(void) const override;

private:
    std::vector<dummy_enum_ptr> enums;
    std::vector<dummy_volume_ptr> volumes;
    std::string device_id;
    std::atomic<bool> keep_going {true};
    std::thread t;

    void animate(void);
};

struct dummy_enum : public ienum_control
{
    dummy_enum(std::string const & name, std::vector<std::string> const & selections);
    std::vector<std::string> get_enum_choices(void) const override;
    unsigned get_selection(void) const override;
    void set_selection(unsigned) override;
    std::string get_name(void) const override;
    void register_enum_changed(void const *, enum_handler_t const &) override;
    void unregister_all(void const *) override;
    void unregister_all(void) override;

private:
    std::string name;
    std::vector<std::string> selections;
    unsigned current_selection {0};
    std::vector<std::pair<void const *, enum_handler_t>> enum_changed;
    std::mutex mutey;
};

struct dummy_volume : public ivolume_control
{
    dummy_volume(std::string const & name, long min_db = -50, long max_db = 50);
    long get_max_db(void) const override;
    long get_min_db(void) const override;
    void set_db(long) override;
    long get_db(void) const override;
    std::string get_name(void) const override;
    void register_volume_changed(void const *, volume_handler_t const &) override;
    void unregister_all(void const *) override;
    void unregister_all(void) override;
    long minimum_step_size(void) const override;
    /* Severity of 1 changes the value "all the way", and 0 "not at all". */
    void change_value(double severity = .01);

private:
    std::string name;
    long const min_db;
    long const max_db;
    long current_db;
    bool going_up;
    std::vector<std::pair<void const *, volume_handler_t>> volume_changed;
    mutable std::mutex mutey;
};

std::pair<std::vector<dummy_enum_ptr>, std::vector<dummy_volume_ptr>> mock_18i8(void);
std::pair<std::vector<dummy_enum_ptr>, std::vector<dummy_volume_ptr>> mock_18i6(void);

idevice_ptr get_dummy_device(decltype(mock_18i8()) const &, std::string const & dev_name, bool animate = false);

} /* namespace scarlett */
