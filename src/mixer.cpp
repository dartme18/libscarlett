#include "stdafx.hpp"

#include "mixer.hpp"
#include "scarlett/exception.hpp"
#include "device.hpp"

namespace scarlett
{

mixer::mixer(idevice_ptr dev)
    : dev {dev}
{
    populate_elements(*this->dev);
}

mixer::~mixer(void)
{
    dev->unregister_all(this);
    INFO("Mixer destroying");
}

void mixer::unregister_all(void)
{
    if (dev)
        dev->unregister_all(this);
}

void mixer::add_usb_output(ienum_ptr const & elem)
{
    elem->register_enum_changed(this, [this](auto & elem, unsigned old_index) {
        decltype(usb_output_assignment_changed) handlers;
        {
            lock_t l {mutey};
            handlers = usb_output_assignment_changed;
        }
        for (auto const & h : handlers)
            h(elem, old_index);
    });
    usb_outputs.push_back(elem);
}

void mixer::add_input_setting(ienum_ptr const & elem)
{
    input_settings.push_back(elem);
    elem->register_enum_changed(this, [this](auto & e, unsigned old_index) {
        decltype(settings_changed) handlers;
        {
            lock_t l {mutey};
            handlers = settings_changed;
        }
        for (auto const & h : handlers)
            h(e, old_index);
    });
}

void mixer::add_clock_setting(ienum_ptr const & elem)
{
    clock_settings.push_back(elem);
    elem->register_enum_changed(this, [this](auto & e, unsigned old_index) {
        decltype(settings_changed) handlers;
        {
            lock_t l {mutey};
            handlers = settings_changed;
        }
        for (auto const & h : handlers)
            h(e, old_index);
    });
}

void mixer::handle_enum_element(ienum_ptr const & elem, std::vector<ienum_ptr> & matrix_inputs,
    std::vector<ienum_ptr> & output_sources)
{
    auto name = elem->get_name();
    if (name.rfind("Input Source", 0) != std::string::npos)
        add_usb_output(elem);
    else if (std::regex_match(name, std::regex {"Matrix [0-9]+ Input"}))
        matrix_inputs.push_back(elem);
    else if (std::regex_match(name, std::regex {"Master [0-9]+[RL] \\(.*"}))
        output_sources.push_back(elem);
    else if (std::regex_match(name, std::regex {"Input [0-9]+ (Impedance|Pad)"}))
        add_input_setting(elem);
    else if (std::regex_match(name, std::regex {".*Clock.*"}))
        add_clock_setting(elem);
    else
        WARN("Unknown element, {}, an enumerated element.", name);
}

void mixer::add_master_volume(ivolume_ptr const & elem)
{
    master_volume = elem;
    elem->register_volume_changed(this, [this](auto & v) {
        decltype(master_volume_changed) handlers;
        {
            lock_t l {mutey};
            handlers = master_volume_changed;
        }
        for (auto const & h : handlers)
            h(v);
    });
}

void mixer::handle_volume_element(ivolume_ptr const & vol, std::vector<ivolume_ptr> & matrix_volumes,
    std::vector<ivolume_ptr> & output_volumes)
{
    std::string name = vol->get_name();
    std::regex r;
    if (std::regex_match(name, std::regex {"Matrix [0-9]+ Mix.*"}))
        matrix_volumes.push_back(vol);
    else if (std::regex_match(name, std::regex {"Master [0-9]+ .*"}) || std::regex_match(name, std::regex {"ADAT [0-9]+.*"}) || name.rfind("Extension Unit", 0) != std::string::npos)
        output_volumes.push_back(vol);
    else if (name.rfind("Master (", 0) != std::string::npos)
        add_master_volume(vol);
    else
        WARN("Unknown volume mixer element, {}.", name);
}

void mixer::set_outputs(std::vector<ienum_ptr> && sources, std::vector<ivolume_ptr> && volumes)
{
    for (auto const & source : sources)
    {
        /* Given a name like  "Master 1R (Monitor) Source", this converts it to
         * "Master 1 (Monitor)" which appears to be sufficient. */
        auto channel {source->get_name()[8]};
        auto sought_name {source->get_name().erase(8, 1)};
        auto end_paren_spot {sought_name.find_last_of(')')};
        sought_name.erase(end_paren_spot + 1);
        for (auto & volume : volumes)
        {
            auto volume_name {volume->get_name()};
            if (volume_name.rfind(sought_name, 0) == std::string::npos)
                continue;
            auto last_end_paren_spot {volume_name.rfind(')')};
            if (last_end_paren_spot == std::string::npos)
                /* They should all have an end paren after the channel number! */
                throw exception {"No end paren after channel number in volume name."};
            if (last_end_paren_spot == 0)
                throw exception {"Volume name begins with end paren!?"};
            auto volume_channel_num {volume_name[last_end_paren_spot - 1]};
            if ((channel == 'R' && volume_channel_num != '1') || (channel == 'L' && volume_channel_num != '0'))
                continue;
            output_assignments.push_back(source);
            output_volumes.push_back(volume);
            source->register_enum_changed(this, [this](auto & elem, unsigned old_index) {
                decltype(output_assignment_changed) handlers;
                {
                    lock_t l {mutey};
                    handlers = output_assignment_changed;
                }
                for (auto const & h : handlers)
                    h(elem, old_index);
            });
            volume->register_volume_changed(this, [this](auto & vol) {
                decltype(output_volume_changed) handlers;
                {
                    lock_t l {mutey};
                    handlers = output_volume_changed;
                }
                for (auto const & h : handlers)
                    h(vol);
            });
        }
    }
}

void mixer::populate_elements(idevice const & dev)
{
    auto enums {dev.get_enums()};
    std::vector<ienum_ptr> matrix_inputs;
    std::vector<ienum_ptr> output_sources;
    for (auto const & e : enums)
        handle_enum_element(e, matrix_inputs, output_sources);
    std::vector<ivolume_ptr> matrix_volumes;
    std::vector<ivolume_ptr> output_volumes;
    auto volumes {dev.get_volumes()};
    for (auto const & v : volumes)
        handle_volume_element(v, matrix_volumes, output_volumes);
    set_outputs(std::move(output_sources), std::move(output_volumes));
    matrix_mixer.set_elements(std::move(matrix_inputs), std::move(matrix_volumes));
}

ivolume_control & mixer::get_master_volume(void)
{
    return *master_volume;
}

ivolume_control const & mixer::get_master_volume(void) const
{
    return *master_volume;
}

imatrix_mixer & mixer::get_matrix(void)
{
    return matrix_mixer;
}

unsigned mixer::get_output_count(void) const
{
    return output_assignments.size();
}

ivolume_control const & mixer::get_output_volume(unsigned out_num) const
{
    if (out_num >= output_volumes.size())
        throw exception {"Output index doesn't exist."};
    return *output_volumes[out_num];
}

ivolume_control & mixer::get_output_volume(unsigned out_num)
{
    if (out_num >= output_volumes.size())
        throw exception {"Output index doesn't exist."};
    return *output_volumes[out_num];
}

ienum_control const & mixer::get_output_assignment(unsigned out_num) const
{
    if (out_num >= output_assignments.size())
        throw exception {"Output index doesn't exist."};
    return *output_assignments[out_num];
}

ienum_control & mixer::get_output_assignment(unsigned out_num)
{
    if (out_num >= output_assignments.size())
        throw exception {"Output index doesn't exist."};
    return *output_assignments[out_num];
}

unsigned mixer::get_usb_output_count(void) const
{
    return usb_outputs.size();
}

ienum_control & mixer::get_usb_output(unsigned num)
{
    if (num >= usb_outputs.size())
        throw exception {"Invalid USB output index."};
    return *usb_outputs[num];
}

ienum_control const & mixer::get_usb_output(unsigned num) const
{
    if (num >= usb_outputs.size())
        throw exception {"Invalid USB output index."};
    return *usb_outputs[num];
}

imatrix_mixer const & mixer::get_matrix(void) const
{
    return matrix_mixer;
}

imixer_ptr get_mixer(idevice_ptr const & dev)
{
    return std::make_unique<mixer>(dev);
}

void mixer::add_usb_output_assignment_listener(selection_handler_t const & handler)
{
    lock_t l {mutey};
    usb_output_assignment_changed.emplace_back(handler);
}

void mixer::add_output_volume_listener(output_volume_handler_t const & handler)
{
    lock_t l {mutey};
    output_volume_changed.push_back(handler);
}

void mixer::add_output_assignment_listener(output_assignment_handler_t const & handler)
{
    lock_t l {mutey};
    output_assignment_changed.push_back(handler);
}

void mixer::add_master_volume_listener(volume_handler_t const & handler)
{
    lock_t l {mutey};
    master_volume_changed.push_back(handler);
}

void mixer::add_settings_listener(selection_handler_t const & handler)
{
    lock_t l {mutey};
    settings_changed.push_back(handler);
}

ienum_control const & mixer::get_from_all_outputs(unsigned num) const
{
    if (num >= get_output_count())
        return get_usb_output(num - get_output_count());
    return get_output_assignment(num);
}

ienum_control & mixer::get_from_all_outputs(unsigned num)
{
    auto const & me {*this};
    auto const & ret {me.get_from_all_outputs(num)};
    return const_cast<ienum_control &>(ret);
}

unsigned mixer::get_all_outputs_count(void) const
{
    return get_output_count() + get_usb_output_count();
}

} /* namespace scarlett */
