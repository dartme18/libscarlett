#include "stdafx.hpp"

#include "volume_control.hpp"
#include "scarlett/exception.hpp"

namespace scarlett
{

/* Without this mutex, it is possible that callbacks are occurring even after calling unregister_all.
 * This also means that one should not call unregister_all from an element change handler (because
 * the mutex is not recursive)! */
static std::mutex unregister_mutex;
using lock_t = std::lock_guard<std::mutex>;

static std::mutex & get_callback_mutex(void)
{
    static std::mutex m;
    return m;
}

static std::lock_guard<std::mutex> lock(void)
{
    return std::lock_guard {get_callback_mutex()};
}

struct volume_callback_t
{
    volume_control * simple;
    snd_mixer_elem_t * elem;
    std::vector<std::pair<void const *, ivolume_control::volume_handler_t>> handlers;
};

struct callback_helper
{
    callback_helper(std::vector<volume_callback_t> & vs)
        : vs {vs}
        , l {lock()}
    {
    }
    std::vector<volume_callback_t> & vs;

private:
    std::lock_guard<std::mutex> l;
};

static callback_helper get_callbacks_registered(void)
{
    static std::vector<volume_callback_t> v;
    return {v};
}

static int volume_control_callback(snd_mixer_elem_t * elem, unsigned)
{
    lock_t {unregister_mutex};
    std::vector<volume_callback_t> vs;
    {
        auto r {get_callbacks_registered()};
        vs = r.vs;
    }
    /* We don't want to hold the lock when we call the callbacks.
     * We copy the callbacks with the lock, release the lock, then call
     * the copies. */
    for (auto const & callback : vs)
    {
        if (callback.elem != elem)
            continue;
        for (auto const & f : callback.handlers)
            f.second(*callback.simple);
    }
    return 0;
}

volume_control::volume_control(void)
    : elem {nullptr}
    , channel {CHANNEL_UNKNOWN}
    , capture {false}
{
}

volume_control::volume_control(snd_mixer_elem_t * elem, snd_mixer_selem_channel_id_t ch, bool capture)
    : elem {elem}
    , channel {ch}
    , capture {capture}
{
}

volume_control::~volume_control(void)
{
    auto r {get_callbacks_registered()};
    for (int i {static_cast<int>(r.vs.size()) - 1}; i >= 0; --i)
    {
        if (r.vs[i].simple != this)
            continue;
        r.vs.erase(r.vs.begin() + i);
    }
}

std::string volume_control::get_name(void) const
{
    return snd_mixer_selem_get_name(elem) + (" (ch. " + std::to_string(channel)) + ")";
}

snd_mixer_selem_channel_id_t volume_control::get_channel(void) const
{
    return channel;
}

long volume_control::get_db(void) const
{
    long ret;
    int err;
    if (capture)
    {
        if ((err = snd_mixer_selem_get_capture_dB(elem, channel, &ret)) < 0)
            throw exception {"Unable to get capture DB: ", snd_strerror(err)};
    }
    else
    {
        if ((err = snd_mixer_selem_get_playback_dB(elem, channel, &ret)) < 0)
            throw exception {"Unable to get playback DB: ", snd_strerror(err)};
    }
    return ret;
}

long volume_control::get_max_db(void) const
{
    long ret;
    long dummy;
    int err;
    if (capture)
    {
        if ((err = snd_mixer_selem_get_capture_dB_range(elem, &dummy, &ret)) < 0)
            throw exception {"Unable to get capture DB: ", snd_strerror(err)};
    }
    else
    {
        if ((err = snd_mixer_selem_get_playback_dB_range(elem, &dummy, &ret)) < 0)
            throw exception {"Unable to get playback DB: ", snd_strerror(err)};
    }
    return ret;
}

long volume_control::get_min_db(void) const
{
    long ret;
    long dummy;
    int err;
    if (capture)
    {
        if ((err = snd_mixer_selem_get_capture_dB_range(elem, &ret, &dummy)) < 0)
            throw exception {"Unable to get capture DB: ", snd_strerror(err)};
    }
    else
    {
        if ((err = snd_mixer_selem_get_playback_dB_range(elem, &ret, &dummy)) < 0)
            throw exception {"Unable to get playback DB: ", snd_strerror(err)};
    }
    return ret;
}

void volume_control::set_db(long val)
{
    int err;
    if (capture)
    {
        if ((err = snd_mixer_selem_set_capture_dB(elem, channel, val, 0)) < 0)
            throw exception {"Unable to set capture DB: ", snd_strerror(err)};
    }
    else
    {
        if ((err = snd_mixer_selem_set_playback_dB(elem, channel, val, 0)) < 0)
            throw exception {"Unable to set playback DB: ", snd_strerror(err)};
    }
    volume_control_callback(elem, 0);
}

void volume_control::register_volume_changed(void const * token, volume_handler_t const & h)
{
    auto r {get_callbacks_registered()};
    auto spot {std::find_if(r.vs.begin(), r.vs.end(), [this](auto const & a) { return a.elem == elem; })};
    if (spot == r.vs.end())
        snd_mixer_elem_set_callback(elem, &volume_control_callback);
    r.vs.push_back(volume_callback_t {this, elem, {{token, h}}});
}

long volume_control::minimum_step_size(void) const
{
    /* This value appears to be hard-coded into the driver code for scarlett. */
    return 100;
}

void volume_control::unregister_all(void)
{
    lock_t {unregister_mutex};
    get_callbacks_registered().vs.clear();
}

void volume_control::unregister_all(void const * token)
{
    lock_t {unregister_mutex};
    auto r {get_callbacks_registered()};
    for (unsigned i {0}; i < r.vs.size(); ++i)
    {
        auto & handlers {r.vs[i].handlers};
        for (unsigned j {0}; j < handlers.size(); ++j)
            if (handlers[j].first == token)
                handlers.erase(handlers.begin() + j--);
    }
}

} /* namespace scarlett */
