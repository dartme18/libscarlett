#include "stdafx.hpp"

#include <gtest/gtest.h>
#include "scarlett/idevice.hpp"
#include "scarlett/imixer.hpp"
#include "scarlett/mixer_wrapper.hpp"
#include "../dummy_device.hpp"
#include "scarlett/ienum_control.hpp"

struct reg : public ::testing::Test
{
    reg(void)
        : elements {scarlett::mock_18i8()}
        , mix {scarlett::get_mixer(get_dummy_device(elements, "dummy:18i8"))}
    {
    }

    scarlett::ivolume_ptr get_volume_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.second)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

    scarlett::ienum_ptr get_enum_element_contains(std::string const & starts)
    {
        for (auto & el : elements.first)
            if (el->get_name().find(starts) != std::string::npos)
                return el;
        return {};
    }

    scarlett::ienum_ptr get_enum_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.first)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

protected:
    decltype(scarlett::mock_18i8()) elements;
    std::unique_ptr<scarlett::imixer> mix;
};

TEST_F(reg, unregister_enum_nullptr)
{
    auto element {get_enum_element_starts_with("Input Source ")};
    bool called {false};
    element->register_enum_changed(nullptr, [&called](auto &, auto) { called = true; });
    element->set_selection(0);
    element->set_selection(1);
    ASSERT_TRUE(called) << "enum changed handler not called properly";
    called = false;
    element->unregister_all(nullptr);
    element->set_selection(0);
    element->set_selection(1);
    ASSERT_FALSE(called) << "Enum change handler not unregistered properly";
}

TEST_F(reg, unregister_enum)
{
    auto element {get_enum_element_starts_with("Input Source ")};
    bool called {false};
    element->register_enum_changed(this, [&called](auto &, auto) { called = true; });
    element->set_selection(0);
    element->set_selection(1);
    ASSERT_TRUE(called) << "enum changed handler not called properly";
    called = false;
    element->unregister_all(this);
    element->set_selection(0);
    element->set_selection(1);
    ASSERT_FALSE(called) << "Enum change handler not unregistered properly";
}

TEST_F(reg, unregister_vol)
{
    auto element {get_volume_element_starts_with("Master 1 ")};
    bool called {false};
    element->register_volume_changed(this, [&called](auto &) { called = true; });
    element->set_db(0);
    element->set_db(1);
    ASSERT_TRUE(called) << "changed handler not called properly";
    called = false;
    element->unregister_all(this);
    element->set_db(0);
    element->set_db(1);
    ASSERT_FALSE(called) << "change handler not unregistered properly";
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
