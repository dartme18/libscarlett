#include "stdafx.hpp"

#include <gtest/gtest.h>
#include "scarlett/idevice.hpp"
#include "scarlett/imixer.hpp"
#include "scarlett/mixer_wrapper.hpp"
#include "../dummy_device.hpp"

struct mixer_wrapper : public ::testing::Test
{
    mixer_wrapper(void)
        : elements {scarlett::mock_18i8()}
        , mixer {scarlett::get_mixer(get_dummy_device(elements, "dummy:18i8"))}
        , wrapper {std::make_unique<scarlett::mixer_wrapper>(*mixer)}
    {
        for (auto & el : elements.first)
            el->set_selection(0);
    }

    scarlett::ivolume_ptr get_volume_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.second)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

    scarlett::ienum_ptr get_enum_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.first)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

    void fail_test(std::string const & error = {})
    {
        FAIL() << error;
    }

    scarlett::ienum_control & get_usb_output_control(unsigned num = 0)
    {
        for (unsigned i {0}; i < mixer->get_usb_output_count(); ++i)
        {
            if (num--)
                continue;
            return mixer->get_usb_output(i);
        }
        throw scarlett::exception {"Could not find USB output."};
    }

    std::string get_usb_output(unsigned num = 0)
    {
        for (auto const & temp_out : wrapper->get_outputs())
        {
            if (wrapper->get_output_volume(temp_out) != scarlett::mixer_wrapper::no_volume)
                continue;
            if (num)
            {
                --num;
                continue;
            }
            return temp_out;
        }
        fail_test("No USB output found for num " + std::to_string(num) + ".");
        return {};
    }

protected:
    decltype(scarlett::mock_18i8()) elements;
    std::unique_ptr<scarlett::imixer> mixer;
    std::unique_ptr<scarlett::mixer_wrapper> wrapper;
};

TEST_F(mixer_wrapper, set_output_volume)
{
    auto outputs {wrapper->get_outputs()};
    ASSERT_GT(outputs.size(), 0) << "Must have at least one output!";
    int const new_vol {10};
    wrapper->set_output_volume(outputs[0], new_vol);
    EXPECT_EQ(wrapper->get_output_volume(outputs[0]), new_vol);
}

TEST_F(mixer_wrapper, mix_volume_get_set_single)
{
    auto outputs {wrapper->get_outputs()};
    ASSERT_GT(outputs.size(), 0) << "Must have at least one output!";
    auto inputs {wrapper->get_mix_inputs()};
    ASSERT_GT(inputs.size(), 0) << "Must have at least one input!";
    int new_vol {1};
    auto in {inputs[0]};
    auto out {outputs[0]};
    EXPECT_FALSE(wrapper->add_input_to_output(out, in)) << "When only one input is assigned to an output, "
                                                           "this function should return false.";
    try
    {
        wrapper->set_mix_volume(out, in, new_vol);
        FAIL() << "Setting a mix volume on a single-input output should throw.";
    }
    catch (scarlett::exception &)
    {
    }
    EXPECT_EQ(scarlett::mixer_wrapper::no_volume, wrapper->get_mix_volume(out, in));
    EXPECT_EQ(wrapper->get_mix_input_max_db(out, in), scarlett::mixer_wrapper::no_volume);
    EXPECT_EQ(wrapper->get_mix_input_min_db(out, in), scarlett::mixer_wrapper::no_volume);
}

TEST_F(mixer_wrapper, mix_volume_get_set_mix)
{
    auto outputs {wrapper->get_outputs()};
    ASSERT_GT(outputs.size(), 0) << "Must have at least one output!";
    auto inputs {wrapper->get_mix_inputs()};
    ASSERT_GT(inputs.size(), 0) << "Must have at least one input!";
    int new_vol {1};
    auto in0 {inputs[1]};
    auto in1 {inputs[0]};
    auto out {outputs.back()};
    EXPECT_FALSE(wrapper->add_input_to_output(out, in0)) << "When only one input is assigned to an output, "
                                                            "this function should return false.";
    EXPECT_TRUE(wrapper->add_input_to_output(out, in1)) << "After adding two inputs, this should return true.";
    wrapper->set_mix_volume(out, in0, new_vol);
    EXPECT_EQ(new_vol, wrapper->get_mix_volume(out, in0));
    --new_vol;
    wrapper->set_mix_volume(out, in0, new_vol);
    EXPECT_EQ(new_vol, wrapper->get_mix_volume(out, in0));
    EXPECT_NE(wrapper->get_mix_input_max_db(out, in0), scarlett::mixer_wrapper::no_volume);
    EXPECT_NE(wrapper->get_mix_input_min_db(out, in0), scarlett::mixer_wrapper::no_volume);
}

TEST_F(mixer_wrapper, mix_volume_changed_callback_internal)
{
    bool called {false};
    auto outputs {wrapper->get_outputs()};
    auto inputs {wrapper->get_mix_inputs()};
    auto const out {outputs.back()};
    auto const in0 {inputs[0]};
    auto const in1 {inputs[1]};
    int const vol {3};
    EXPECT_FALSE(wrapper->add_input_to_output(out, in0)) << "Adding first input.";
    EXPECT_TRUE(wrapper->add_input_to_output(out, in1)) << "Adding first input.";
    wrapper->add_mix_volume_listener([&called, out, in0, vol](auto const & o, auto const & i, int v) {
        if (o != out || i != in0)
            return;
        EXPECT_EQ(v, vol) << "Volume doesn't match.";
        EXPECT_FALSE(called) << "Already called! Maybe a second call isn't fatal, though....";
        called = true;
    });
    wrapper->set_mix_volume(out, in0, vol);
    EXPECT_TRUE(called) << "Change handler never called.";
}

TEST_F(mixer_wrapper, mix_volume_changed_callback_external)
{
    bool called {false};
    auto outputs {wrapper->get_outputs()};
    auto inputs {wrapper->get_mix_inputs()};
    auto const out {outputs[0]};
    auto const in0 {inputs[0]};
    auto const in1 {inputs[1]};
    int const vol {3};
    /* We need two inputs so that a mix is used rather than direct assignment.*/
    wrapper->add_input_to_output(out, in0);
    wrapper->add_input_to_output(out, in1);
    wrapper->add_mix_volume_listener([&called, out, in0, vol](auto const & o, auto const & i, int) {
        if (o != out || i != in0)
            return;
        called = true;
    });
    for (auto & el : elements.second)
    {
        if (el->get_name().rfind("Matrix ", 0) == std::string::npos)
            continue;
        el->set_db(el->get_db() == 0 ? 1 : 0);
    }
    EXPECT_TRUE(called) << "Change handler never called.";
}

TEST_F(mixer_wrapper, master_volume)
{
    wrapper->set_master_volume(5);
    EXPECT_EQ(5, wrapper->get_master_volume()) << "Failed to set master volume";
    wrapper->set_master_volume(4);
    EXPECT_EQ(4, wrapper->get_master_volume()) << "Failed to set master volume";
}

TEST_F(mixer_wrapper, master_volume_listener_internal)
{
    int const new_vol {1};
    wrapper->set_master_volume(new_vol - 1);
    bool called {false};
    wrapper->add_master_volume_listener([new_vol, &called](auto evt) {
        EXPECT_EQ(new_vol, evt) << "Master volume callback incorrect volume. ";
        EXPECT_FALSE(called) << "The callback was already called.";
        called = true;
    });
    wrapper->set_master_volume(new_vol);
    EXPECT_TRUE(called) << "The master volume callback wasn't called.";
}

TEST_F(mixer_wrapper, output_volume_listener_internal)
{
    int const new_vol {1};
    auto output {wrapper->get_outputs()[0]};
    wrapper->set_output_volume(output, new_vol - 1);
    bool called {false};
    wrapper->add_output_volume_listener([new_vol, &called, output](auto const & out, auto evt) {
        EXPECT_EQ(new_vol, evt) << "Output volume callback incorrect volume. ";
        EXPECT_FALSE(called) << "The callback was already called.";
        EXPECT_EQ(output, out) << "Wrong callback called.";
        called = true;
    });
    wrapper->set_output_volume(output, new_vol);
    EXPECT_TRUE(called) << "The output volume callback wasn't called.";
}

TEST_F(mixer_wrapper, mix_usb_output_added_listener_internal)
{
    auto inputs {wrapper->get_mix_inputs()};
    auto output {get_usb_output()};
    auto input {wrapper->get_mix_inputs()[0]};
    bool called {false};
    wrapper->add_mix_output_listener([&output, &input, &called](auto const & out, auto const & in, bool added) {
        EXPECT_EQ(out, output) << "Unexpected output adjusted";
        EXPECT_EQ(in, input) << "Unexpected input adjusted";
        EXPECT_TRUE(added) << "Input should have been added.";
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    wrapper->add_input_to_output(output, input);
    EXPECT_TRUE(called) << "Mix output callback not called.";
}

TEST_F(mixer_wrapper, mix_output_added_listener_internal)
{
    auto inputs {wrapper->get_mix_inputs()};
    auto output {wrapper->get_outputs()[0]};
    auto input {wrapper->get_mix_inputs()[0]};
    bool called {false};
    wrapper->add_mix_output_listener([&output, &input, &called](auto const & out, auto const & in, bool added) {
        EXPECT_EQ(out, output) << "Unexpected output adjusted";
        EXPECT_EQ(in, input) << "Unexpected input adjusted";
        EXPECT_TRUE(added) << "Input should have been added.";
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    wrapper->add_input_to_output(output, input);
    EXPECT_TRUE(called) << "Mix output callback not called.";
}

TEST_F(mixer_wrapper, mix_output_removed_listener_internal)
{
    auto output {wrapper->get_outputs()[0]};
    auto input {wrapper->get_mix_inputs()[0]};
    bool called {false};
    wrapper->add_input_to_output(output, input);
    wrapper->add_mix_output_listener([&output, &input, &called](auto const & out, auto const & in, bool added) {
        EXPECT_EQ(out, output) << "Unexpected output adjusted";
        EXPECT_EQ(in, input) << "Unexpected input adjusted";
        EXPECT_FALSE(added) << "Input should have been removed.";
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    wrapper->remove_input_from_output(output, input);
    EXPECT_TRUE(called) << "Mix output callback not called.";
}

/* When changing an output's mix from A to B, any volumes that change
 * should be reported. */
TEST_F(mixer_wrapper, output_mix_changed_external)
{
    auto mixer_input0 {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input0.get(), nullptr) << "Couldn't find matrix element.";
    auto mixer_vol0 {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol0.get(), nullptr) << "Couldn't find volume element.";
    auto mixer_vol1 {get_volume_element_starts_with("Matrix 01 Mix B")};
    EXPECT_NE(mixer_vol1.get(), nullptr) << "Couldn't find volume element.";
    auto master_output {get_enum_element_starts_with("Master 1L")};
    EXPECT_NE(master_output.get(), nullptr) << "Couldn't find output.";
    mixer_input0->set_selection(1);
    master_output->set_selection("Mix A");
    int expected_vol {1};
    bool called {false};
    auto expected_out {master_output->get_name()};
    auto expected_in {mixer_input0->get_str_selection()};
    mixer_vol0->set_db(expected_vol - 1);
    mixer_vol1->set_db(expected_vol);
    wrapper->add_mix_volume_listener([&called, expected_out, expected_in, expected_vol](auto const & out_name, auto const & in_name, int new_vol) {
        EXPECT_EQ(out_name, expected_out);
        EXPECT_EQ(in_name, expected_in);
        EXPECT_EQ(expected_vol, new_vol);
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    master_output->set_selection("Mix B");
    EXPECT_TRUE(called) << "Mix volume changed handler never called.";
}

TEST_F(mixer_wrapper, output_mix_added_new_external)
{
    /*The output changes from a specific input to a mix without that input.*/
    auto mixer_input {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input.get(), nullptr);
    auto mixer_vol {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol.get(), nullptr);
    auto master_output {get_enum_element_starts_with("Master 1L")};
    EXPECT_NE(master_output.get(), nullptr);
    unsigned const added_input {1};
    unsigned const removed_input {2};
    mixer_input->set_selection(added_input);
    master_output->set_selection(removed_input);
    auto expected_out {master_output->get_name()};
    auto added_input_name {mixer_input->get_str_selection()};
    auto removed_input_name {master_output->get_str_selection()};
    bool called_added {false};
    bool called_removed {false};
    wrapper->add_mix_output_listener([&called_added, &called_removed, expected_out, added_input_name, removed_input_name](auto const & out_name, auto const & in_name, bool added) {
        EXPECT_EQ(out_name, expected_out);
        if (added)
        {
            EXPECT_EQ(in_name, added_input_name);
            EXPECT_FALSE(called_added);
            called_added = true;
        }
        else
        {
            EXPECT_EQ(in_name, removed_input_name);
            EXPECT_FALSE(called_removed);
            called_removed = true;
        }
    });
    master_output->set_selection("Mix A");
    EXPECT_TRUE(called_removed);
    EXPECT_TRUE(called_added);
}

TEST_F(mixer_wrapper, output_mix_added_existing_external)
{
    /*The output changes from a specific input to a mix with that input.*/
    auto mixer_input {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input.get(), nullptr);
    auto mixer_vol {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol.get(), nullptr);
    auto master_output {get_enum_element_starts_with("Master 1L")};
    EXPECT_NE(master_output.get(), nullptr);
    unsigned const input_selection {1};
    int const expected_vol {1};
    mixer_input->set_selection(input_selection);
    master_output->set_selection(input_selection);
    mixer_vol->set_db(expected_vol);
    auto expected_out {master_output->get_name()};
    auto expected_input {mixer_input->get_str_selection()};
    bool called {false};
    wrapper->add_mix_volume_listener([&called, expected_out, expected_input, expected_vol](auto const & out_name, auto const & in_name, int new_vol) {
        EXPECT_EQ(out_name, expected_out);
        EXPECT_EQ(in_name, expected_input);
        EXPECT_EQ(new_vol, expected_vol);
        EXPECT_FALSE(called);
        called = true;
    });
    master_output->set_selection("Mix A");
    EXPECT_TRUE(called);
}

TEST_F(mixer_wrapper, output_mix_removed_external)
{
    /*The output changes from a mix to a specific input that was not already on the mix.*/
    auto mixer_input {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input.get(), nullptr) << "Couldn't find matrix element.";
    auto mixer_vol {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol.get(), nullptr) << "Couldn't find volume element.";
    auto master_output {get_enum_element_starts_with("Master 1L")};
    EXPECT_NE(master_output.get(), nullptr) << "Couldn't find output.";
    mixer_input->set_selection(1);
    master_output->set_selection("Mix A");
    mixer_vol->set_db(1);
    bool got_removed {false};
    bool got_added {false};
    unsigned const new_selection {2};
    auto expected_out {master_output->get_name()};
    auto expected_removed {mixer_input->get_str_selection()};
    auto expected_added {mixer_input->get_enum_choices()[new_selection]};
    wrapper->add_mix_output_listener([&got_removed, &got_added, expected_out, expected_removed, expected_added](auto const & out_name, auto const & in_name, bool added) {
        if (added)
        {
            EXPECT_FALSE(got_added) << "Added more than one input.";
            got_added = true;
            EXPECT_EQ(in_name, expected_added);
        }
        else
        {
            EXPECT_FALSE(got_removed) << "Removed more than one input.";
            got_removed = true;
            EXPECT_EQ(in_name, expected_removed);
        }
        EXPECT_EQ(out_name, expected_out);
    });
    master_output->set_selection(new_selection);
    EXPECT_TRUE(got_removed);
    EXPECT_TRUE(got_added);
}

TEST_F(mixer_wrapper, output_mix_removed_existing_external)
{
    /*The output changes from a mix to a specific input that was already on the mix.*/
    auto mixer_input {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input.get(), nullptr) << "Couldn't find matrix element.";
    auto mixer_vol {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol.get(), nullptr) << "Couldn't find volume element.";
    auto master_output {get_enum_element_starts_with("Master 1L")};
    EXPECT_NE(master_output.get(), nullptr) << "Couldn't find output.";
    mixer_input->set_selection(1);
    master_output->set_selection("Mix A");
    mixer_vol->set_db(1);
    bool called {false};
    auto expected_out {master_output->get_name()};
    auto expected_input {mixer_input->get_str_selection()};
    wrapper->add_mix_volume_listener([&called, expected_out, expected_input](auto const & out_name, auto const & in_name, int new_vol) {
        EXPECT_EQ(new_vol, scarlett::mixer_wrapper::no_volume);
        EXPECT_EQ(out_name, expected_out);
        EXPECT_EQ(in_name, expected_input);
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    master_output->set_selection(mixer_input->get_str_selection());
    EXPECT_TRUE(called) << "Mix volume changed handler never called.";
}

TEST_F(mixer_wrapper, output_mix_turned_off_external)
{
    /* set an output to a mix
     * Make sure one of the mix inputs is not "Off".
     * set output Off.
     * Ensure callback is called to remove whatever the input was.
     */
    auto mixer_input {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input.get(), nullptr) << "Couldn't find matrix element.";
    auto mixer_vol {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol.get(), nullptr) << "Couldn't find volume element.";
    auto master_output {get_enum_element_starts_with("Master 1L")};
    EXPECT_NE(master_output.get(), nullptr) << "Couldn't find output.";
    mixer_input->set_selection(1);
    master_output->set_selection("Mix A");
    mixer_vol->set_db(1);
    bool called {false};
    auto expected_out {master_output->get_name()};
    auto expected_removed {mixer_input->get_str_selection()};
    wrapper->add_mix_output_listener([&called, expected_out, expected_removed](auto const & out_name, auto const & in_name, bool added) {
        EXPECT_FALSE(added);
        EXPECT_EQ(out_name, expected_out);
        EXPECT_EQ(in_name, expected_removed);
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    master_output->set_selection(scarlett::mixer_wrapper::off_option);
    EXPECT_TRUE(called) << "Mix volume changed handler never called.";
}

/* When changing USB output's mix from A to B, any volumes that change
 * should be reported. */
TEST_F(mixer_wrapper, usb_output_mix_changed_external)
{
    auto mixer_input0 {get_enum_element_starts_with("Matrix 01")};
    EXPECT_NE(mixer_input0.get(), nullptr) << "Couldn't find matrix element.";
    auto mixer_vol0 {get_volume_element_starts_with("Matrix 01 Mix A")};
    EXPECT_NE(mixer_vol0.get(), nullptr) << "Couldn't find volume element.";
    auto mixer_vol1 {get_volume_element_starts_with("Matrix 01 Mix B")};
    EXPECT_NE(mixer_vol1.get(), nullptr) << "Couldn't find volume element.";
    auto & output {get_usb_output_control()};
    mixer_input0->set_selection(1);
    output.set_selection("Mix A");
    int expected_vol {1};
    bool called {false};
    auto expected_out {output.get_name()};
    auto expected_in {mixer_input0->get_str_selection()};
    mixer_vol0->set_db(expected_vol - 1);
    mixer_vol1->set_db(expected_vol);
    wrapper->add_mix_volume_listener([&called, expected_out, expected_in, expected_vol](auto const & out_name, auto const & in_name, int new_vol) {
        EXPECT_EQ(out_name, expected_out);
        EXPECT_EQ(in_name, expected_in);
        EXPECT_EQ(expected_vol, new_vol);
        EXPECT_FALSE(called) << "Callback called more than once.";
        called = true;
    });
    output.set_selection("Mix B");
    EXPECT_TRUE(called) << "Mix volume changed handler never called.";
}

TEST_F(mixer_wrapper, mix_input_removed_external)
{
    auto output {wrapper->get_outputs()[0]};
    auto removed_input0 {"PCM 2"};
    auto removed_input1 {"PCM 3"};
    wrapper->add_input_to_output(output, removed_input0);
    wrapper->add_input_to_output(output, removed_input1);
    /*Get the matrix input that was used for the output. */
    scarlett::ienum_ptr input_ptr;
    for (auto & el : elements.first)
    {
        if (el->get_str_selection() == removed_input0)
        {
            input_ptr = el;
            break;
        }
    }
    ASSERT_NE(input_ptr.get(), nullptr) << "Couldn't find the correct matrix input.";
    bool called {false};
    wrapper->add_mix_output_listener([&output, &removed_input0, &called](auto const & out, auto const & in, bool added) {
        EXPECT_EQ(out, output);
        EXPECT_EQ(in, removed_input0);
        EXPECT_FALSE(added);
        EXPECT_FALSE(called);
        called = true;
    });
    input_ptr->set_selection(scarlett::mixer_wrapper::off_option);
    EXPECT_TRUE(called) << "Callback not called.";
}

TEST_F(mixer_wrapper, mix_input_removed_from_usb_external)
{
    auto output {get_usb_output()};
    auto removed_input0 {"PCM 2"};
    auto removed_input1 {"PCM 3"};
    wrapper->add_input_to_output(output, removed_input0);
    wrapper->add_input_to_output(output, removed_input1);
    /*Get the matrix input that was used for the output. */
    scarlett::ienum_ptr input_ptr;
    for (auto & el : elements.first)
    {
        if (el->get_str_selection() == removed_input0)
        {
            input_ptr = el;
            break;
        }
    }
    ASSERT_NE(input_ptr.get(), nullptr) << "Couldn't find the correct matrix input.";
    bool called {false};
    wrapper->add_mix_output_listener([&output, &removed_input0, &called](auto const & out, auto const & in, bool added) {
        EXPECT_EQ(out, output);
        EXPECT_EQ(in, removed_input0);
        EXPECT_FALSE(added);
        EXPECT_FALSE(called);
        called = true;
    });
    input_ptr->set_selection(scarlett::mixer_wrapper::off_option);
    EXPECT_TRUE(called) << "Callback not called.";
}

TEST_F(mixer_wrapper, mix_input_added_external)
{
    auto output {get_enum_element_starts_with(wrapper->get_outputs()[0])};
    auto input0 {"PCM 2"};
    auto input1 {"PCM 3"};
    auto added_input {"PCM 4"};
    wrapper->add_input_to_output(output->get_name(), input0);
    wrapper->add_input_to_output(output->get_name(), input1);
    /*Get an unused matrix input. */
    scarlett::ienum_ptr input_ptr;
    for (auto & el : elements.first)
    {
        if (el->get_name().rfind("Matrix", 0) == std::string::npos)
            continue;
        if (el->get_str_selection() == scarlett::mixer_wrapper::off_option)
        {
            input_ptr = el;
            break;
        }
    }
    ASSERT_NE(input_ptr.get(), nullptr) << "Couldn't find the correct matrix input.";
    scarlett::ivolume_ptr input_volume;
    for (auto & el : elements.second)
    {
        if (el->get_name().substr(0, 9) != input_ptr->get_name().substr(0, 9))
            continue;
        if (el->get_name().substr(10, 5) != output->get_str_selection())
            continue;
        input_volume = el;
        break;
    }
    ASSERT_NE(input_volume.get(), nullptr) << "Couldn't find matrix volume.";
    bool called {false};
    wrapper->add_mix_output_listener([&output, &added_input, &called](auto const & out, auto const & in, bool added) {
        EXPECT_EQ(out, output->get_name());
        EXPECT_EQ(in, added_input);
        EXPECT_TRUE(added);
        EXPECT_FALSE(called);
        called = true;
    });
    input_volume->set_db(input_volume->get_min_db() + 1);
    input_ptr->set_selection(added_input);
    EXPECT_TRUE(called) << "Callback not called.";
}

TEST_F(mixer_wrapper, mix_input_duplicate_external)
{
    /* This likely shouldn't be implemented until the wrapper knows
     * what inputs each output has. */
    ERROR("Not implemented.");
}

TEST_F(mixer_wrapper, usb_output_assignments)
{
    auto inputs {wrapper->get_mix_inputs()};
    auto output {get_usb_output()};
    EXPECT_FALSE(wrapper->add_input_to_output(output, inputs.back())) << "First input assignment.";
    EXPECT_TRUE(wrapper->add_input_to_output(output, inputs[0])) << "Second input assignment.";
    auto input_assignments {wrapper->get_inputs(output)};
    EXPECT_EQ(2, input_assignments.size());
    auto spot {std::find_if(input_assignments.begin(), input_assignments.end(), [&inputs](auto const & assignment) {
        return assignment.input == inputs[0];
    })};
    EXPECT_NE(spot, input_assignments.end()) << "The 0th input should be assigned to the output.";
    spot = std::find_if(input_assignments.begin(), input_assignments.end(), [&inputs](auto const & assignment) {
        return assignment.input == inputs.back();
    });
    EXPECT_NE(spot, input_assignments.end()) << "The last input should be assigned to the output.";
}

TEST_F(mixer_wrapper, remove_input_from_output)
{
    auto outputs {wrapper->get_outputs()};
    auto inputs {wrapper->get_mix_inputs()};
    auto in0 {inputs.back()};
    auto in1 {inputs[0]};
    auto in2 {inputs[1]};
    auto out {outputs.back()};
    EXPECT_FALSE(wrapper->add_input_to_output(out, in0)) << "When only one input is assigned to an output, "
                                                            "this function should return false.";
    EXPECT_TRUE(wrapper->add_input_to_output(out, in1)) << "After adding two inputs, this should return true.";
    EXPECT_TRUE(wrapper->add_input_to_output(out, in2)) << "Should still return true";
    EXPECT_TRUE(wrapper->remove_input_from_output(out, in1)) << "Since two inputs remain, this should return true.";
    EXPECT_FALSE(wrapper->remove_input_from_output(out, in2)) << "Since two inputs remain, this should return true.";
    EXPECT_FALSE(wrapper->remove_input_from_output(out, in0));
    auto assignments {wrapper->get_inputs(out)};
    EXPECT_EQ(0, assignments.size()) << "There should be only one input now.";
}

TEST_F(mixer_wrapper, remove_input_from_usb_output)
{
    auto inputs {wrapper->get_mix_inputs()};
    auto in0 {inputs.back()};
    auto in1 {inputs[0]};
    auto out {get_usb_output()};
    EXPECT_FALSE(wrapper->add_input_to_output(out, in0)) << "When only one input is assigned to an output, "
                                                            "this function should return false.";
    EXPECT_TRUE(wrapper->add_input_to_output(out, in1)) << "After adding two inputs, this should return true.";
    EXPECT_FALSE(wrapper->remove_input_from_output(out, in1)) << "Back down to only one input.";
    auto assignments {wrapper->get_inputs(out)};
    EXPECT_EQ(1, assignments.size()) << "There should be only one input now.";
    EXPECT_EQ(in0, assignments[0].input) << "The wrapper removed the wrong input?";
}

TEST_F(mixer_wrapper, usb_output_mix_volume)
{
    auto inputs {wrapper->get_mix_inputs()};
    int new_vol {1};
    auto in0 {inputs.back()};
    auto in1 {inputs[0]};
    auto out {get_usb_output()};
    EXPECT_FALSE(wrapper->add_input_to_output(out, in0));
    EXPECT_TRUE(wrapper->add_input_to_output(out, in1));
    wrapper->set_mix_volume(out, in0, new_vol);
    EXPECT_EQ(new_vol, wrapper->get_mix_volume(out, in0));
    --new_vol;
    wrapper->set_mix_volume(out, in0, new_vol);
    EXPECT_EQ(new_vol, wrapper->get_mix_volume(out, in0));
}

TEST_F(mixer_wrapper, outputs_on_different_mixes)
{
    /* After adding two outputs to mixes, changing the volume on one output should
     * not change the volume on the other output. */
    auto inputs {wrapper->get_mix_inputs()};
    auto output0 {get_usb_output()};
    auto output1 {get_usb_output(1)};
    wrapper->add_input_to_output(output0, inputs.front());
    wrapper->add_input_to_output(output0, inputs.at(1));
    wrapper->add_input_to_output(output1, inputs.front());
    wrapper->add_input_to_output(output1, inputs.at(1));
    auto new_vol {1};
    wrapper->set_mix_volume(output0, inputs.front(), new_vol - 1);
    wrapper->set_mix_volume(output1, inputs.front(), new_vol - 1);
    EXPECT_EQ(wrapper->get_mix_volume(output0, inputs.front()), new_vol - 1);
    EXPECT_EQ(wrapper->get_mix_volume(output1, inputs.front()), new_vol - 1);
    wrapper->set_mix_volume(output0, inputs.front(), new_vol);
    EXPECT_EQ(wrapper->get_mix_volume(output0, inputs.front()), new_vol);
    EXPECT_EQ(wrapper->get_mix_volume(output1, inputs.front()), new_vol - 1) << "Changing the volume on one mix output should not change the volume on the other output.";
}

TEST_F(mixer_wrapper, keep_track_of_input_assignments)
{
    /* After adding two outputs to mixes, adding an input to one output
     * should not add it to the second output. */
    auto inputs {wrapper->get_mix_inputs()};
    auto output0 {get_usb_output()};
    auto output1 {get_usb_output(1)};
    wrapper->add_input_to_output(output0, inputs.front());
    wrapper->add_input_to_output(output0, inputs.at(1));
    wrapper->add_input_to_output(output1, inputs.front());
    wrapper->add_input_to_output(output1, inputs.at(2));
    auto inputs0 {wrapper->get_inputs(output0)};
    auto spot0 {std::find_if(inputs0.begin(), inputs0.end(), [input = inputs.at(2)](auto const & s) {
        return s.input == input;
    })};
    EXPECT_EQ(spot0, inputs0.end());
    auto inputs1 {wrapper->get_inputs(output1)};
    auto spot1 {std::find_if(inputs1.begin(), inputs1.end(), [input = inputs.at(1)](auto const & s) {
        return s.input == input;
    })};
    EXPECT_EQ(spot1, inputs1.end());
}

TEST_F(mixer_wrapper, keep_track_of_input_volumes)
{
    /* When a mix input externally changes volume from minimum volume to not minimum,
     * it should be added to the mix. */
    auto inputs {wrapper->get_mix_inputs()};
    auto output0 {get_usb_output()};
    auto output1 {get_usb_output(1)};
    auto new_input {inputs.at(2)};
    wrapper->add_input_to_output(output0, inputs.front());
    wrapper->add_input_to_output(output0, inputs.at(1));
    wrapper->add_input_to_output(output1, inputs.front());
    wrapper->add_input_to_output(output1, new_input);
    auto inputs0 {wrapper->get_inputs(output0)};
    auto spot0 {std::find_if(inputs0.begin(), inputs0.end(), [new_input](auto const & s) {
        return s.input == new_input;
    })};
    EXPECT_EQ(spot0, inputs0.end());
    auto inputs1 {wrapper->get_inputs(output1)};
    auto spot1 {std::find_if(inputs1.begin(), inputs1.end(), [input = inputs.at(1)](auto const & s) {
        return s.input == input;
    })};
    EXPECT_EQ(spot1, inputs1.end());
}

TEST_F(mixer_wrapper, add_second_input)
{
    auto inputs {wrapper->get_mix_inputs()};
    auto output {get_usb_output()};
    wrapper->add_input_to_output(output, inputs.front());
    auto added_input {inputs.at(1)};
    bool called {false};
    /* It might be permissible for a callback to be issued to remove the original input, then another two
     * callbacks to add the two inputs back. */
    wrapper->add_mix_output_listener([&added_input, &output, &called](auto const & out, auto const & in_name, bool added) {
        EXPECT_EQ(output, out);
        EXPECT_EQ(in_name, added_input);
        EXPECT_TRUE(added);
        EXPECT_FALSE(called);
        called = true;
    });
    wrapper->add_input_to_output(output, added_input);
    EXPECT_EQ(wrapper->get_inputs(output).size(), 2);
    EXPECT_TRUE(called);
}

TEST_F(mixer_wrapper, add_third_input)
{
    auto inputs {wrapper->get_mix_inputs()};
    auto output {get_usb_output()};
    wrapper->add_input_to_output(output, inputs.front());
    wrapper->add_input_to_output(output, inputs.at(1));
    auto added_input {inputs.at(2)};
    bool called {false};
    wrapper->add_mix_output_listener([&added_input, &output, &called](auto const & out, auto const & in_name, bool added) {
        EXPECT_EQ(output, out);
        EXPECT_EQ(in_name, added_input);
        EXPECT_TRUE(added);
        EXPECT_FALSE(called);
        called = true;
    });
    wrapper->add_input_to_output(output, added_input);
    EXPECT_TRUE(called);
}

TEST_F(mixer_wrapper, reclaim_unused_mixes)
{
    /* When a mix isn't being used, we should be able to grab its outputs. */
    for (auto & el : elements.first)
        el->set_selection(1);
    wrapper->add_input_to_output(get_usb_output(), wrapper->get_mix_inputs().at(2));
    /* If the above line fails, it will throw. */
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
