#include "stdafx.hpp"

#include <gtest/gtest.h>
#include "scarlett/idevice.hpp"
#include "scarlett/imixer.hpp"
#include "scarlett/mixer_wrapper.hpp"
#include "../dummy_device.hpp"
#include "scarlett/ienum_control.hpp"

struct mixer : public ::testing::Test
{
    mixer(void)
        : elements {scarlett::mock_18i8()}
        , mix {scarlett::get_mixer(get_dummy_device(elements, "dummy:18i8"))}
    {
    }

    scarlett::ivolume_ptr get_volume_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.second)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

    scarlett::ienum_ptr get_enum_element_contains(std::string const & starts)
    {
        for (auto & el : elements.first)
            if (el->get_name().find(starts) != std::string::npos)
                return el;
        return {};
    }

    scarlett::ienum_ptr get_enum_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.first)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

protected:
    decltype(scarlett::mock_18i8()) elements;
    std::unique_ptr<scarlett::imixer> mix;
};

TEST_F(mixer, usb_callback)
{
    auto usb_element {get_enum_element_starts_with("Input Source ")};
    ASSERT_NE(usb_element.get(), nullptr) << "Couldn't find usb element.";
    auto called {false};
    unsigned new_val {0};
    unsigned old_val {1};
    usb_element->set_selection(old_val);
    auto element_name {usb_element->get_name()};
    mix->add_usb_output_assignment_listener([&called, &element_name, old_val](auto & el, unsigned old) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, el.get_name()) << "Callback called for wrong element.";
        EXPECT_EQ(old_val, old) << "The old value in the callback was incorrect.";
        called = true;
    });
    usb_element->set_selection(new_val);
    EXPECT_TRUE(called) << "Output assignment callback not called on USB output.";
}

TEST_F(mixer, output_volume_callback)
{
    auto volume_element {get_volume_element_starts_with("Master 1 ")};
    ASSERT_NE(volume_element.get(), nullptr) << "Couldn't find volume element.";
    auto called {false};
    volume_element->set_db(1);
    auto element_name {volume_element->get_name()};
    mix->add_output_volume_listener([&called, &element_name](auto & out) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, out.get_name()) << "Callback called for wrong element.";
        called = true;
    });
    volume_element->set_db(0);
    EXPECT_TRUE(called) << "Output assignment callback not called on volume output.";
}

TEST_F(mixer, output_enum_callback)
{
    auto enum_element {get_enum_element_starts_with("Master 1L")};
    ASSERT_NE(enum_element.get(), nullptr) << "Couldn't find enum element.";
    auto called {false};
    unsigned new_val {0};
    unsigned old_val {1};
    enum_element->set_selection(old_val);
    auto element_name {enum_element->get_name()};
    mix->add_output_assignment_listener([&called, &element_name, old_val](auto & out, unsigned old) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, out.get_name()) << "Callback called for wrong element.";
        EXPECT_EQ(old_val, old) << "Old value was incorrect in callback.";
        called = true;
    });
    enum_element->set_selection(new_val);
    EXPECT_TRUE(called) << "Output assignment callback not called on enum output.";
}

TEST_F(mixer, master_volume_listener)
{
    auto volume_element {get_volume_element_starts_with("Master (")};
    ASSERT_NE(volume_element.get(), nullptr) << "Couldn't find master volume element.";
    auto called {false};
    volume_element->set_db(1);
    auto element_name {volume_element->get_name()};
    mix->add_master_volume_listener([&called, &element_name](auto & el) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, el.get_name()) << "Callback called for wrong element.";
        called = true;
    });
    volume_element->set_db(0);
    EXPECT_TRUE(called) << "Callback not called on master volume control.";
}

TEST_F(mixer, input_settings_callback)
{
    auto el {get_enum_element_starts_with("Input 1 Impe")};
    ASSERT_NE(el.get(), nullptr) << "Couldn't find input settings enum element.";
    auto called {false};
    unsigned new_val {0};
    unsigned old_val {1};
    el->set_selection(old_val);
    auto element_name {el->get_name()};
    mix->add_settings_listener([&called, &element_name, old_val](auto & el, unsigned old) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, el.get_name()) << "Callback called for wrong element.";
        EXPECT_EQ(old, old_val) << "Old value incorrect in callback.";
        called = true;
    });
    el->set_selection(new_val);
    EXPECT_TRUE(called) << "Input setting callback not called.";
}

TEST_F(mixer, clock_settings_callback)
{
    auto el {get_enum_element_contains("Clock")};
    ASSERT_NE(el.get(), nullptr) << "Couldn't find clock settings enum element.";
    auto called {false};
    unsigned new_val {0};
    unsigned old_val {1};
    el->set_selection(old_val);
    auto element_name {el->get_name()};
    mix->add_settings_listener([&called, &element_name, old_val](auto & el, unsigned old) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, el.get_name()) << "Callback called for wrong element.";
        EXPECT_EQ(old, old_val) << "Old value incorrect in callback.";
        called = true;
    });
    el->set_selection(new_val);
    EXPECT_TRUE(called) << "Clock setting callback not called.";
}

TEST_F(mixer, matrix_input_callback)
{
    auto el {get_enum_element_starts_with("Matrix 0")};
    ASSERT_NE(el.get(), nullptr) << "Couldn't find matrix enum element.";
    auto called {false};
    unsigned new_val {0};
    unsigned old_val {1};
    el->set_selection(old_val);
    auto element_name {el->get_name()};
    mix->get_matrix().add_input_listener([&called, &element_name, old_val](auto & input, unsigned old) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, input.get_matrix_input().get_name()) << "Callback called for wrong element.";
        EXPECT_EQ(old, old_val) << "Old value incorrect in callback.";
        called = true;
    });
    el->set_selection(new_val);
    EXPECT_TRUE(called) << "Matrix input callback not called.";
}

TEST_F(mixer, matrix_volume_callback)
{
    auto volume_element {get_volume_element_starts_with("Matrix 0")};
    ASSERT_NE(volume_element.get(), nullptr) << "Couldn't find matrix volume element.";
    auto called {false};
    volume_element->set_db(1);
    auto element_name {volume_element->get_name()};
    mix->get_matrix().add_volume_listener([&called, &element_name](auto & el) {
        EXPECT_FALSE(called) << "Callback called a second time?";
        EXPECT_EQ(element_name, el.get_volume_control().get_name()) << "Callback called for wrong element.";
        called = true;
    });
    volume_element->set_db(0);
    EXPECT_TRUE(called) << "Matrix volume callback not called.";
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
