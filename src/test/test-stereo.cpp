#include "stdafx.hpp"

#include <gtest/gtest.h>
#include "scarlett/idevice.hpp"
#include "scarlett/imixer.hpp"
#include "scarlett/mixer_wrapper.hpp"
#include "../dummy_device.hpp"

struct stereo : public ::testing::Test
{
    stereo(void)
        : elements {scarlett::mock_18i8()}
        , mixer {scarlett::get_mixer(get_dummy_device(elements, "dummy:18i8"))}
        , wrapper {std::make_unique<scarlett::mixer_wrapper>(*mixer)}
    {
        for (auto & el : elements.first)
            el->set_selection(0);
    }

    scarlett::ivolume_ptr get_volume_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.second)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

    scarlett::ienum_ptr get_enum_element_starts_with(std::string const & starts)
    {
        for (auto & el : elements.first)
            if (el->get_name().rfind(starts, 0) != std::string::npos)
                return el;
        return {};
    }

    void fail_test(std::string const & error = {})
    {
        FAIL() << error;
    }

    scarlett::ienum_control & get_usb_output_control(unsigned num = 0)
    {
        for (unsigned i {0}; i < mixer->get_usb_output_count(); ++i)
        {
            if (num--)
                continue;
            return mixer->get_usb_output(i);
        }
        throw scarlett::exception {"Could not find USB output."};
    }

    std::string get_usb_output(unsigned num = 0)
    {
        for (auto const & temp_out : wrapper->get_outputs())
        {
            if (wrapper->get_output_volume(temp_out) != scarlett::mixer_wrapper::no_volume)
                continue;
            if (num)
            {
                --num;
                continue;
            }
            return temp_out;
        }
        fail_test("No USB output found for num " + std::to_string(num) + ".");
        return {};
    }

protected:
    decltype(scarlett::mock_18i8()) elements;
    std::unique_ptr<scarlett::imixer> mixer;
    std::unique_ptr<scarlett::mixer_wrapper> wrapper;
};

TEST_F(stereo, create_output_stereo_pair)
{
    /* Kick the tires a bit. */
    auto outputs {wrapper->get_outputs()};
    auto output_stereo_pair_0 {wrapper->create_output_stereo_pair(outputs[0], outputs[1])};
    auto output_stereo_pair_1 {wrapper->create_output_stereo_pair(outputs[2], outputs[3])};
    auto temp_pair0 {wrapper->get_output_stereo_pair(output_stereo_pair_0)};
    auto temp_pair1 {wrapper->get_output_stereo_pair(output_stereo_pair_1)};
    EXPECT_EQ(temp_pair0.element0, outputs[0]) << "Technically, the value returned by get_output_stereo_pair can be in any order";
    EXPECT_EQ(temp_pair0.element1, outputs[1]);
    EXPECT_EQ(temp_pair0.num, output_stereo_pair_0);
    EXPECT_EQ(temp_pair1.element0, outputs[2]);
    EXPECT_EQ(temp_pair1.element1, outputs[3]);
    EXPECT_EQ(temp_pair1.num, output_stereo_pair_1);
    EXPECT_NE(output_stereo_pair_0, output_stereo_pair_1) << "The wrapper gave two output_stereo outputs that are the same";
    auto max_db {wrapper->get_output_stereo_pair_max_db(output_stereo_pair_0)};
    auto min_db {wrapper->get_output_stereo_pair_min_db(output_stereo_pair_0)};
    wrapper->set_output_stereo_pair_volume(output_stereo_pair_0, max_db);
    EXPECT_EQ(wrapper->get_output_stereo_pair_volume(output_stereo_pair_0), max_db);
    wrapper->set_output_stereo_pair_volume(output_stereo_pair_0, min_db);
    EXPECT_EQ(wrapper->get_output_stereo_pair_volume(output_stereo_pair_0), min_db);
    wrapper->set_output_stereo_pair_volume(output_stereo_pair_0, 0);
    wrapper->set_output_stereo_pair_pan(output_stereo_pair_0, -1);
    EXPECT_EQ(wrapper->get_output_stereo_pair_pan(output_stereo_pair_0), -1);
    wrapper->set_output_stereo_pair_pan(output_stereo_pair_0, 0);
    EXPECT_EQ(wrapper->get_output_stereo_pair_pan(output_stereo_pair_0), 0);
    wrapper->set_output_stereo_pair_pan(output_stereo_pair_0, 1);
    EXPECT_EQ(wrapper->get_output_stereo_pair_pan(output_stereo_pair_0), 1);
    wrapper->set_output_stereo_pair_pan(output_stereo_pair_0, .1);
    EXPECT_LT(std::abs(wrapper->get_output_stereo_pair_pan(output_stereo_pair_0) - .1), .0001);
    wrapper->set_output_stereo_pair_volume(output_stereo_pair_0, -2);
    EXPECT_EQ(wrapper->get_output_stereo_pair_volume(output_stereo_pair_0), -2);
}

/** When a stereo pair changes (volume, pan, etc.) a callback should be issued. */
TEST_F(stereo, output_changed_callbacks)
{
    auto outputs {wrapper->get_outputs()};
    auto pair {wrapper->create_output_stereo_pair(outputs[0], outputs[1])};
    bool called {false};
    bool calling_volume {true};
    int const new_vol {-2};
    double const new_pan {-.3};
    bool got_volume {false};
    bool got_pan {false};
    wrapper->add_output_stereo_pair_changed_listener([expected_pair = pair, &got_volume, &got_pan, new_vol, new_pan, &calling_volume, &called](auto const & pair, int vol, double pan) {
        /* The callback can be called multiple times because there are multiple
             * volume elements driving it. */
        //EXPECT_FALSE(called) << "Callback called twice";
        called = true;
        EXPECT_EQ(expected_pair, pair) << "Wrong pair callback called";
        if (calling_volume)
        {
            if (std::abs(new_vol - vol) < 2)
                got_volume = true;
        }
        else
        {
            if (std::abs(new_pan - pan) < .001)
                got_pan = true;
        }
    });
    wrapper->set_output_stereo_pair_volume(pair, new_vol);
    EXPECT_TRUE(called) << "Callback not called for volume change";
    called = false;
    calling_volume = false;
    wrapper->set_output_stereo_pair_pan(pair, new_pan);
    EXPECT_TRUE(called) << "Callback not called for pan change";
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
