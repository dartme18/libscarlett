#include "stdafx.hpp"

#include <gtest/gtest.h>
#include "scarlett/idevice.hpp"
#include "scarlett/imixer.hpp"
#include "scarlett/mixer_wrapper.hpp"
#include "dummy_device.hpp"

struct dummy_18i8 : public ::testing::Test
{
    dummy_18i8(void)
        : mixer {scarlett::get_mixer(scarlett::get_dummy_device("18i8"))}
    {
    }

protected:
    decltype(scarlett::get_mixer(scarlett::get_dummy_device("hey"))) mixer;
};

TEST_F(dummy_18i8, outputs)
{
    std::vector<std::string> expected_outputs {"Master 1 (Monitor) (ch. 0)", "Master 1 (Monitor) (ch. 1)", "Master 2 (Headphone 1) (ch. 0)", "Master 2 (Headphone 1) (ch. 1)", "Master 3 (Headphone 2) (ch. 0)", "Master 3 (Headphone 2) (ch. 1)", "Master 4 (SPDIF) (ch. 0)", "Master 4 (SPDIF) (ch. 1)"};
    for (unsigned output_num {0}; output_num < mixer->get_output_count(); ++output_num)
    {
        auto const & output {mixer->get_output_volume(output_num)};
        auto spot {std::find(expected_outputs.begin(), expected_outputs.end(), output.get_name())};
        EXPECT_NE(spot, expected_outputs.end()) << "Found unexpected output: " << output.get_name() << '.';
        expected_outputs.erase(spot);
    }
    EXPECT_EQ(expected_outputs.size(), 0) << "Some outputs not accounted for, including " << expected_outputs[0] << '.';
}

TEST_F(dummy_18i8, usb_outputs)
{
    for (unsigned out_num {0}; out_num < mixer->get_usb_output_count(); ++out_num)
    {
        auto const & usb_out {mixer->get_usb_output(out_num)};
        std::vector<std::string> expected_choices {"Off", "PCM 1", "PCM 2", "PCM 3", "PCM 4", "PCM 5", "PCM 6", "PCM 7", "PCM 8", "Analog 1", "Analog 2", "Analog 3", "Analog 4", "Analog 5", "Analog 6", "Analog 7", "Analog 8", "SPDIF 1", "SPDIF 2", "ADAT 1", "ADAT 2", "ADAT 3", "ADAT 4", "ADAT 5", "ADAT 6", "ADAT 7", "ADAT 8", "Mix A", "Mix B", "Mix C", "Mix D", "Mix E", "Mix F", "Mix G", "Mix H"};
        for (auto const & choice : usb_out.get_enum_choices())
        {
            auto spot {std::find(expected_choices.begin(), expected_choices.end(), choice)};
            EXPECT_NE(spot, expected_choices.end()) << "Found unexpected output: " << choice;
            expected_choices.erase(spot);
        }
        EXPECT_EQ(expected_choices.size(), 0) << "Some choices not accounted for in usb output " << usb_out.get_name() << " including " << expected_choices[0] << '.';
    }
}

TEST_F(dummy_18i8, master_volume)
{
    auto master_name {mixer->get_master_volume().get_name()};
    EXPECT_EQ(master_name, "Master (ch. 0)");
}

TEST_F(dummy_18i8, matrix_inputs)
{
    auto const & matrix {mixer->get_matrix()};
    for (unsigned input_num {0}; input_num < matrix.get_input_count(); ++input_num)
    {
        auto const & input {matrix.get_matrix_input(input_num)};
        std::string expected_name_prefix {"Matrix "};
        // Add prefixed zero for the cases 0-9 (the number will be incremented later)
        expected_name_prefix += (input_num < 9) ? "0" : "";
        expected_name_prefix += std::to_string(input_num + 1);
        auto expected_input_name {expected_name_prefix + " Input"};
        EXPECT_EQ(expected_input_name, input.get_input_name()) << "Expected name to be " << expected_input_name << ", but found " << input.get_input_name() << '.';
        for (unsigned mix_num {0}; mix_num < matrix.get_mix_count(); ++mix_num)
        {
            auto volume_name {matrix.get_matrix_volume(input_num, mix_num).get_volume_control().get_name()};
            auto expected_volume_name {expected_name_prefix + " Mix "};
            expected_volume_name += 'A' + mix_num;
            expected_volume_name += " (ch. 0)";
            EXPECT_EQ(expected_volume_name, volume_name) << "Expected volume name to be " << expected_volume_name << ", but found " << volume_name << ".";
        }
    }
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
