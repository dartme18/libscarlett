#pragma once

#include <vector>
#include "scarlett/ivolume_control.hpp"
#include "scarlett/ienum_control.hpp"
#include "scarlett/imatrix_mixer.hpp"
#include "scarlett/idevice.hpp"

namespace scarlett
{

struct device : public idevice
{
    device(std::string const & dev);
    ~device(void);
    std::vector<std::shared_ptr<ienum_control>> get_enums(void) const override;
    std::vector<std::shared_ptr<ivolume_control>> get_volumes(void) const override;
    std::string get_device_id(void) const override;
    /* Do not call this from a change callback. It will deadlock. */
    void unregister_all(void const *) override;
    void unregister_all(void) override;
    void register_device_disconnected(void const * token, disconnect_handler_t const &) override;
    bool is_disconnected(void) const override;

private:
    std::vector<ienum_ptr> enums;
    std::vector<ivolume_ptr> volumes;
    snd_mixer_t * mixer {nullptr};
    std::string device_id {"none"};
    std::mutex event_mutey;
    std::vector<std::pair<void const *, disconnect_handler_t>> disconnect_handlers;
    std::atomic_bool disconnected {true};
    int err;
    /* To signal the alsa event thread to keep going. */
    std::atomic_bool keep_running {true};
    std::atomic_bool inited {false};
    std::thread alsa_event_thread;

    void pump_alsa_events(void);
    void init(std::string const &);
    void populate_elements(void);
};

} /* namespace scarlett */
