#pragma once

#include <vector>
#include "matrix.hpp"
#include "scarlett/ienum_control.hpp"
#include "scarlett/ivolume_control.hpp"
#include "scarlett/imixer.hpp"
#include "scarlett/idevice.hpp"

namespace scarlett
{

struct mixer : public imixer
{
    mixer(idevice_ptr);
    ~mixer(void);
    unsigned get_usb_output_count(void) const override;
    ienum_control & get_usb_output(unsigned) override;
    ienum_control const & get_usb_output(unsigned) const override;
    unsigned get_output_count(void) const override;
    ienum_control & get_output_assignment(unsigned) override;
    ienum_control const & get_output_assignment(unsigned) const override;
    unsigned get_all_outputs_count(void) const override;
    ienum_control & get_from_all_outputs(unsigned) override;
    ienum_control const & get_from_all_outputs(unsigned) const override;
    ivolume_control & get_output_volume(unsigned) override;
    ivolume_control const & get_output_volume(unsigned) const override;
    ivolume_control & get_master_volume(void) override;
    ivolume_control const & get_master_volume(void) const override;
    imatrix_mixer & get_matrix(void) override;
    imatrix_mixer const & get_matrix(void) const override;
    void unregister_all(void) override;

    void add_usb_output_assignment_listener(selection_handler_t const &) override;
    void add_output_assignment_listener(output_assignment_handler_t const &) override;
    void add_output_volume_listener(output_volume_handler_t const &) override;
    void add_master_volume_listener(volume_handler_t const &) override;
    void add_settings_listener(selection_handler_t const &) override;

private:
    using lock_t = std::lock_guard<std::mutex>;
    idevice_ptr dev;
    std::vector<ienum_ptr> input_settings;
    std::vector<ienum_ptr> clock_settings;
    std::vector<ienum_ptr> output_assignments;
    std::vector<ivolume_ptr> output_volumes;
    std::vector<ienum_ptr> usb_outputs;
    ivolume_ptr master_volume;
    matrix matrix_mixer;
    std::vector<selection_handler_t> usb_output_assignment_changed;
    std::vector<output_assignment_handler_t> output_assignment_changed;
    std::vector<output_volume_handler_t> output_volume_changed;
    std::vector<volume_handler_t> master_volume_changed;
    std::vector<selection_handler_t> settings_changed;
    std::mutex mutey;

    void handle_enum_element(ienum_ptr const &, std::vector<ienum_ptr> &, std::vector<ienum_ptr> &);
    void handle_volume_element(ivolume_ptr const &, std::vector<ivolume_ptr> &, std::vector<ivolume_ptr> &);
    void set_outputs(std::vector<ienum_ptr> &&, std::vector<ivolume_ptr> &&);
    void populate_elements(idevice const &);
    void add_usb_output(ienum_ptr const &);
    void add_master_volume(ivolume_ptr const &);
    void add_input_setting(ienum_ptr const &);
    void add_clock_setting(ienum_ptr const &);
};

} /* namespace scarlett */
