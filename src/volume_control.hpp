#pragma once

#include "scarlett/ivolume_control.hpp"

namespace scarlett
{

struct volume_control : public ivolume_control
{
    volume_control(void);
    volume_control(snd_mixer_elem_t *, snd_mixer_selem_channel_id_t, bool capture);
    ~volume_control(void);
    std::string get_name(void) const override;
    snd_mixer_selem_channel_id_t get_channel(void) const;
    long get_db(void) const override;
    long get_max_db(void) const override;
    long get_min_db(void) const override;
    void set_db(long) override;
    void register_volume_changed(void const * token, volume_handler_t const &) override;
    void unregister_all(void const *) override;
    void unregister_all(void) override;
    long minimum_step_size(void) const override;

    static volume_control const invalid_volume_control;

private:
    /* C APIs aren't const correct */
    mutable snd_mixer_elem_t * elem;
    snd_mixer_selem_channel_id_t channel;
    bool capture;
};

} /* namespace scarlett */
