#include "stdafx.hpp"

#include "enum_control.hpp"
#include "scarlett/exception.hpp"

namespace scarlett
{

/* Without this mutex, it is possible that callbacks are occurring even after calling unregister_all.
 * This also means that one should not call unregister_all from an element change handler (because
 * the mutex is not recursive)! */
static std::mutex unregister_mutex;
using lock_t = std::lock_guard<std::mutex>;

static std::mutex & get_callback_mutex(void)
{
    static std::mutex m;
    return m;
}

static std::lock_guard<std::mutex> lock(void)
{
    return std::lock_guard {get_callback_mutex()};
}

struct enum_callback_t
{
    enum_control * simple;
    snd_mixer_elem_t * elem;
    std::vector<std::pair<void const *, ienum_control::enum_handler_t>> handlers;
};

struct callback_helper
{
    callback_helper(std::vector<enum_callback_t> & vs)
        : vs {vs}
        , l {lock()}
    {
    }
    std::vector<enum_callback_t> & vs;

private:
    std::lock_guard<std::mutex> l;
};

static callback_helper get_callbacks_registered(void)
{
    static std::vector<enum_callback_t> v;
    return {v};
}

static int enum_control_callback(snd_mixer_elem_t * elem, unsigned)
{
    lock_t {unregister_mutex};
    std::vector<enum_callback_t> callbacks;
    {
        auto r {get_callbacks_registered()};
        callbacks = r.vs;
    }
    for (auto const & callback : callbacks)
    {
        if (callback.elem != elem)
            continue;
        callback.simple->fire_change(callback.handlers);
    }
    return 0;
}

enum_control::enum_control(void)
    : elem {nullptr}
    , old_val {0}
{
}

enum_control::enum_control(snd_mixer_elem_t * elem)
    : elem {elem}
    , old_val {get_selection()}
{
}

enum_control::~enum_control(void)
{
    auto r {get_callbacks_registered()};
    for (int i {static_cast<int>(r.vs.size()) - 1}; i >= 0; --i)
    {
        if (r.vs[i].simple != this)
            continue;
        r.vs.erase(r.vs.begin() + i);
    }
}

std::string enum_control::get_name(void) const
{
    return snd_mixer_selem_get_name(elem);
}

std::vector<std::string> enum_control::get_enum_choices(void) const
{
    if (!snd_mixer_selem_is_enumerated(elem))
        return {};
    std::vector<std::string> ret;
    auto num {snd_mixer_selem_get_enum_items(elem)};
    if (num < 0)
    {
        ERROR("Error getting number of enum items: %s.", snd_strerror(num));
        return {};
    }
    for (int i = 0; i < num; ++i)
    {
        char enum_name[254];
        int err;
        if ((err = snd_mixer_selem_get_enum_item_name(elem, i, sizeof(enum_name) - 1, enum_name)) < 0)
        {
            ERROR("Error getting enum item %d: %s.", i, snd_strerror(err));
            return {};
        }
        ret.push_back(enum_name);
    }
    return ret;
}

void enum_control::set_selection(unsigned val)
{
    int err;
    if ((err = snd_mixer_selem_set_enum_item(elem, CHANNEL_0, val)) < 0)
        throw exception {std::string {"unable to set enum item: "} + snd_strerror(err)};
}

unsigned enum_control::get_selection(void) const
{
    int err;
    unsigned ret;
    if ((err = snd_mixer_selem_get_enum_item(elem, CHANNEL_0, &ret)) < 0)
        throw exception {std::string {"Unable to set enum item: "} + snd_strerror(err)};
    return ret;
}
void ienum_control::set_selection(std::string const & val)
{
    auto choices {get_enum_choices()};
    auto spot {std::find(choices.begin(), choices.end(), val)};
    if (spot == choices.end())
        throw exception {"Selection " + val + " invalid for enum " + get_name() + "."};
    set_selection(std::distance(choices.begin(), spot));
}

std::string ienum_control::get_str_selection(void) const
{
    return get_enum_choices().at(get_selection());
}

void enum_control::register_enum_changed(void const * token, enum_handler_t const & handler)
{
    auto r {get_callbacks_registered()};
    auto spot {std::find_if(r.vs.begin(), r.vs.end(), [this](auto const & a) { return a.elem == elem; })};
    if (spot == r.vs.end())
        snd_mixer_elem_set_callback(elem, &enum_control_callback);
    r.vs.push_back({this, elem, {{token, handler}}});
}

void enum_control::fire_change(std::vector<std::pair<void const *, enum_handler_t>> const & handlers)
{
    for (auto const & h : handlers)
        h.second(*this, old_val);
    old_val = get_selection();
}

void enum_control::unregister_all(void)
{
    lock_t {unregister_mutex};
    get_callbacks_registered().vs.clear();
}

void enum_control::unregister_all(void const * token)
{
    lock_t {unregister_mutex};
    auto r {get_callbacks_registered()};
    for (unsigned i {0}; i < r.vs.size(); ++i)
    {
        auto & handlers {r.vs[i].handlers};
        for (unsigned j {0}; j < handlers.size(); ++j)
            if (handlers[j].first == token)
                handlers.erase(handlers.begin() + j--);
    }
}

} /* namespace scarlett */
