#pragma once

#include "enum_control.hpp"
#include "volume_control.hpp"
#include "scarlett/imatrix_mixer.hpp"

namespace scarlett
{

struct matrix_input : public imatrix_input
{
    matrix_input(unsigned input, ienum_control & assignment);
    unsigned get_input_num(void) const override;
    ienum_control & get_matrix_input(void) override;
    ienum_control const & get_matrix_input(void) const override;
    std::string get_input_name(void) const override;

private:
    unsigned const input_num;
    ienum_control & control;
};

struct matrix_volume : public imatrix_volume
{
    matrix_volume(unsigned input, unsigned mix, ivolume_control & volume);
    unsigned get_input_num(void) const override;
    unsigned get_mix_num(void) const override;
    ivolume_control & get_volume_control(void) override;
    ivolume_control const & get_volume_control(void) const override;

private:
    unsigned const input;
    unsigned const mix;
    ivolume_control & control;
};

struct matrix : public imatrix_mixer
{
    ~matrix(void);
    void set_elements(std::vector<ienum_ptr> &&, std::vector<ivolume_ptr> &&);
    unsigned get_input_count(void) const override;
    unsigned get_mix_count(void) const override;
    imatrix_volume & get_matrix_volume(unsigned input_num, unsigned mix) override;
    imatrix_volume const & get_matrix_volume(unsigned input_num, unsigned mix) const override;
    imatrix_input & get_matrix_input(unsigned) override;
    imatrix_input const & get_matrix_input(unsigned) const override;
    void add_volume_listener(volume_handler_t const &) override;
    void add_input_listener(input_handler_t const &) override;

private:
    using lock_t = std::lock_guard<std::mutex>;
    void set_matrix_volumes();
    void set_inputs();
    std::vector<imatrix_volume_ptr> volumes;
    std::vector<imatrix_input_ptr> inputs;
    std::vector<volume_handler_t> volume_changed;
    std::vector<input_handler_t> input_changed;
    std::vector<ienum_ptr> enum_ptrs;
    std::vector<ivolume_ptr> volume_ptrs;
    std::mutex mutey;
};

} /* namespace scarlett */
