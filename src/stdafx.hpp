#include <algorithm>
#include <alsa/asoundlib.h>
#include <regex>
#include <chrono>
#include <random>
#include <mutex>
#include <cctype>
#include <unordered_map>
#include <atomic>
#include <thread>
#include <map>
#include <vector>
#include <limits>
#include <cmath>
#include <memory>
#include <ctime>
#include <chrono>
#include <optional>
#include "logging.hpp"

#define CHANNEL_0 SND_MIXER_SCHN_FRONT_LEFT
#define CHANNEL_1 SND_MIXER_SCHN_FRONT_RIGHT
#define CHANNEL_UNKNOWN SND_MIXER_SCHN_UNKNOWN
