#pragma once

#include "scarlett/ienum_control.hpp"

namespace scarlett
{

struct enum_control : ienum_control
{
    enum_control(void);
    enum_control(enum_control const &) = delete;
    enum_control(enum_control &&) = delete;
    enum_control(snd_mixer_elem_t *, snd_mixer_selem_channel_id_t, bool capture);
    ~enum_control(void);
    enum_control & operator=(enum_control const &) = delete;
    enum_control & operator=(enum_control &&) = delete;
    enum_control(snd_mixer_elem_t *);
    std::string get_name(void) const override;
    std::vector<std::string> get_enum_choices(void) const override;
    unsigned get_selection(void) const override;
    void set_selection(unsigned) override;
    void register_enum_changed(void const *, enum_handler_t const &) override;
    /* Do not call this from a change callback. It will deadlock. */
    void unregister_all(void const *) override;
    void unregister_all(void) override;
    void fire_change(std::vector<std::pair<void const *, enum_handler_t>> const &);

private:
    /* The C APIs are not const correct */
    mutable snd_mixer_elem_t * elem;
    unsigned old_val;
};

} /* namespace scarlett */
