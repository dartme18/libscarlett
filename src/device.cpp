#include "stdafx.hpp"

#include "device.hpp"
#include "scarlett/exception.hpp"
#include "volume_control.hpp"
#include "enum_control.hpp"

namespace scarlett
{

using lock_t = std::lock_guard<std::mutex>;

device::device(std::string const & dev)
    : alsa_event_thread {[this, &dev] { init(dev); }}
{
    while (!inited)
    {
        /* We'll wait for the privates to be fully populated before returning so there
         * isn't a race condition retrieving the control elements.*/
        std::this_thread::sleep_for(std::chrono::milliseconds {1});
    }
}

device::~device(void)
{
    keep_running = false;
    alsa_event_thread.join();
    DEBUG("destroying device... joined ALSA event thread.");
    if (mixer)
        snd_mixer_close(mixer);
}

snd_mixer_t * connect_mixer(std::string const & name, std::string & device_id)
{
    snd_ctl_card_info_t * card_info;
    snd_ctl_card_info_alloca(&card_info);
    snd_ctl_t * ctl;
    int err;
    if ((err = snd_ctl_open(&ctl, name.c_str(), 0)) < 0)
    {
        ERROR("Device {}: error opening: {}.", name, snd_strerror(err));
        return nullptr;
    }
    INFO("opened sound control {}.", snd_ctl_name(ctl));
    if ((err = snd_ctl_card_info(ctl, card_info)) < 0)
    {
        ERROR("Device {}: error retrieving info: {}.", name, snd_strerror(err));
        return nullptr;
    }
    auto real_name {snd_ctl_card_info_get_name(card_info)};
    INFO("  real name is {}.", real_name);
    INFO("  id is {}.", snd_ctl_card_info_get_id(card_info));
    INFO("  driver is {}.", snd_ctl_card_info_get_driver(card_info));
    INFO("  longname is {}.", snd_ctl_card_info_get_longname(card_info));
    INFO("  mixername is {}.", snd_ctl_card_info_get_mixername(card_info));
    INFO("  components is {}.", snd_ctl_card_info_get_components(card_info));
    snd_ctl_close(ctl);
    if (!real_name)
    {
        ERROR("Device {} has no name.", name);
        return nullptr;
    }
    device_id = real_name;
    snd_mixer_t * mixer;
    if ((err = snd_mixer_open(&mixer, 0)) < 0)
    {
        ERROR("Device {} error getting mixer: {}.", name, snd_strerror(err));
        return nullptr;
    }
    if ((err = snd_mixer_attach(mixer, name.c_str())) < 0)
    {
        ERROR("Device {} error attaching mixer: {}.", name, snd_strerror(err));
        return nullptr;
    }
    if ((err = snd_mixer_selem_register(mixer, nullptr, nullptr)) < 0)
    {
        ERROR("Device {} error registering mixer: {}.", name, snd_strerror(err));
        return nullptr;
    }
    if ((err = snd_mixer_load(mixer)) < 0)
    {
        ERROR("Device {} error loading mixer: {}.", name, snd_strerror(err));
        return nullptr;
    }
    DEBUG("Mixer loaded.");
    return mixer;
}

void device::init(std::string const & name)
{
    mixer = connect_mixer(name, device_id);
    if (!mixer)
        throw scarlett::exception {"Unable to connect to mixer."};
    populate_elements();
    inited = true;
    disconnected = false;
    pump_alsa_events();
}

void device::pump_alsa_events(void)
{
    while (keep_running)
    {
        int err;
        if ((err = snd_mixer_wait(mixer, 15)) < 0)
            ERROR("Error waiting for alsa events: {}.", snd_strerror(err));
        err = snd_mixer_handle_events(mixer);
        if (err < 0)
            ERROR("Error handling events: {}.", snd_strerror(err));
        if (err == -19)
        {
            disconnected = true;
            {
                lock_t {event_mutey};
                for (auto const & h : disconnect_handlers)
                    h.second();
            }
            unregister_all();
            break;
        }
    }
}

void device::populate_elements(void)
{
    for (auto elem {snd_mixer_first_elem(mixer)}; elem; elem = snd_mixer_elem_next(elem))
    {
        if (!snd_mixer_selem_is_active(elem))
        {
            WARN("Element {} is not active; skipping.", (void *)elem);
            continue;
        }
        if (snd_mixer_selem_is_enumerated(elem))
        {
            auto name {snd_mixer_selem_get_name(elem)};
            if (std::regex_match(name, std::regex {".*Clock.*"}))
            {
                INFO("Skipping element {} because we don't use it.", name);
                continue;
            }
            enums.push_back(std::make_shared<enum_control>(elem));
        }
        else
        {
            long ret;
            /* Some elements don't work. ADAT 2 and Extension Unit on my unit. */
            auto err {snd_mixer_selem_get_playback_dB(elem, CHANNEL_0, &ret)};
            if (err < 0)
            {
                INFO("Skipping Element {} because I can't get its playback volume.", snd_mixer_selem_get_name(elem));
                continue;
            }
            volumes.push_back(std::make_shared<volume_control>(volume_control {elem, CHANNEL_0, /*capture*/ false}));
            if (snd_mixer_selem_has_playback_channel(elem, CHANNEL_1))
                volumes.push_back(std::make_shared<volume_control>(volume_control {elem, CHANNEL_1, /*capture*/ false}));
        }
    }
}

std::vector<ienum_ptr> device::get_enums(void) const
{
    return enums;
}

std::vector<ivolume_ptr> device::get_volumes(void) const
{
    return volumes;
}

idevice_ptr get_device(std::string const & device_name)
{
    return std::make_shared<device>(device_name);
}

void device::unregister_all(void)
{
    for (auto const & e : enums)
        e->unregister_all();
    for (auto const & v : volumes)
        v->unregister_all();
    lock_t {event_mutey};
    disconnect_handlers.clear();
}

void device::unregister_all(void const * token)
{
    for (auto const & e : enums)
        e->unregister_all(token);
    for (auto const & v : volumes)
        v->unregister_all(token);
    lock_t {event_mutey};
    for (unsigned spot {0}; spot < disconnect_handlers.size(); ++spot)
        if (disconnect_handlers.at(spot).first == token)
            disconnect_handlers.erase(disconnect_handlers.begin() + spot--);
}

std::string create_dummy_code(std::string const & device_name)
{
    std::string device_id;
    auto mixer {connect_mixer(device_name, device_id)};
    if (!mixer)
        return "";
    std::ostringstream output;
    output << "std::unique_ptr<idevice> mock_" << device_id << "(void)\n";
    output << "std::vector<dummy_enum_ptr> enums;\n";
    output << "std::vector<dummy_volume_ptr> volumes;\n";
    for (auto elem {snd_mixer_first_elem(mixer)}; elem; elem = snd_mixer_elem_next(elem))
    {
        auto name {snd_mixer_selem_get_name(elem)};
        if (!snd_mixer_selem_is_active(elem))
        {
            WARN("Element {} is not active; skipping.", name);
            continue;
        }
        if (snd_mixer_selem_is_enumerated(elem))
        {
            output << "enums.push_back(std::make_shared<dummy_enum>(\"";
            output << name;
            output << "\", std::vector<std::string> {";
            unsigned const name_size {5000};
            char temp_name[name_size];
            for (int i {0}; i < snd_mixer_selem_get_enum_items(elem); ++i)
            {
                snd_mixer_selem_get_enum_item_name(elem, i, name_size, temp_name);
                output << "\"" << temp_name << "\"";
                if (i < snd_mixer_selem_get_enum_items(elem) - 1)
                    output << ", ";
                else
                    output << "}";
            }
            output << "));\n";
        }
        else
        {
            output << "volumes.push_back(std::make_shared<dummy_volume>(\"";
            output << name << " (ch. 0)\", ";
            long min, max;
            snd_mixer_selem_get_playback_dB_range(elem, &min, &max);
            output << min << ", " << max << "));\n";
            if (!snd_mixer_selem_is_playback_mono(elem))
            {
                output << "volumes.push_back(std::make_shared<dummy_volume>(\"";
                output << name << " (ch. 1)\", ";
                output << min << ", " << max << "));\n";
            }
        }
    }
    output << "return {enums, volumes};\n";
    return output.str();
}

std::string inspect(std::string const & device_name)
{
    std::string device_id;
    auto mixer {connect_mixer(device_name, device_id)};
    if (!mixer)
        return "";
    std::ostringstream output;
    for (auto elem {snd_mixer_first_elem(mixer)}; elem; elem = snd_mixer_elem_next(elem))
    {
        auto name {snd_mixer_selem_get_name(elem)};
        if (!snd_mixer_selem_is_active(elem))
        {
            WARN("Element {} is not active; skipping.", name);
            continue;
        }
        output << "Element " << name;
        output << " is " << (snd_mixer_selem_is_enumerated(elem) ? "enumerated" : "simple");
        output << ", " << (snd_mixer_selem_is_playback_mono(elem) ? "playback mono" : "playback not mono");
        output << ", " << (snd_mixer_selem_is_capture_mono(elem) ? "capture mono" : "capture not mono");
        output << "; playback channel 0 " << (snd_mixer_selem_has_playback_channel(elem, CHANNEL_0) ? "yes" : "no");
        output << "; playback channel 1 " << (snd_mixer_selem_has_playback_channel(elem, CHANNEL_1) ? "yes" : "no");
        output << "; playback channel 2 " << (snd_mixer_selem_has_playback_channel(elem, SND_MIXER_SCHN_REAR_LEFT) ? "yes" : "no");
        if (snd_mixer_selem_is_enumerated(elem))
        {
            unsigned const name_size {5000};
            char temp_name[name_size];
            std::string enums;
            for (int i {0}; i < snd_mixer_selem_get_enum_items(elem); ++i)
            {
                snd_mixer_selem_get_enum_item_name(elem, i, name_size, temp_name);
                enums += temp_name;
                enums += ",";
            }
            output << "; enum values " << enums;
            output << "; playback " << (snd_mixer_selem_is_enum_playback(elem) ? "yes" : "no");
            output << "; capture " << (snd_mixer_selem_is_enum_capture(elem) ? "yes" : "no");
            output << '\n';
        }
        else
        {
            long min, max;
            snd_mixer_selem_get_playback_dB_range(elem, &min, &max);
            output << "; max playback db " << max << "; min playback db " << min;
            output << '\n';
        }
    }
    return output.str();
}

std::string device::get_device_id(void) const
{
    return device_id;
}

void device::register_device_disconnected(void const * token, disconnect_handler_t const & d)
{
    lock_t {event_mutey};
    disconnect_handlers.emplace_back(token, d);
}

bool device::is_disconnected(void) const
{
    return disconnected;
}

} /* namespace scarlett */
