#pragma once

#include <vector>
#include "ivolume_control.hpp"
#include "ienum_control.hpp"
#include "imatrix_mixer.hpp"

namespace scarlett
{

using idevice_ptr = std::shared_ptr<struct idevice>;

struct idevice
{
    using disconnect_handler_t = std::function<void(void)>;
    virtual ~idevice(void) {};
    virtual std::vector<std::shared_ptr<ienum_control>> get_enums(void) const = 0;
    virtual std::vector<std::shared_ptr<ivolume_control>> get_volumes(void) const = 0;
    virtual std::string get_device_id(void) const = 0;
    virtual void unregister_all(void const *) = 0;
    virtual void unregister_all(void) = 0;
    virtual void register_device_disconnected(void const * token, disconnect_handler_t const &) = 0;
    virtual bool is_disconnected(void) const = 0;
};

/* Attempts to open the specified device. */
idevice_ptr get_device(std::string const & device_name);

/* If no device type is specified, the library chooses a card type to emulate.
* The device name can be terminated with ":a" to animate the device. This causes the volume to
* fluctuate randomly. */
idevice_ptr get_dummy_device(std::string const & device_type = "18i6");

/* Returns information about the alsa elements associated with the specified device. */
std::string inspect(std::string const & device_name);

std::string create_dummy_code(std::string const & dev_name);

} /* namespace scarlett */
