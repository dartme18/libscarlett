#pragma once

#include "ivolume_control.hpp"
#include "ienum_control.hpp"

namespace scarlett
{

using imatrix_input_ptr = std::shared_ptr<struct imatrix_input>;
struct imatrix_input
{
    virtual ~imatrix_input(void) {};
    virtual unsigned get_input_num(void) const = 0;
    virtual ienum_control & get_matrix_input(void) = 0;
    virtual ienum_control const & get_matrix_input(void) const = 0;
    virtual std::string get_input_name(void) const = 0;
};

using imatrix_volume_ptr = std::shared_ptr<struct imatrix_volume>;
struct imatrix_volume
{
    virtual ~imatrix_volume(void) {};
    virtual unsigned get_input_num(void) const = 0;
    virtual unsigned get_mix_num(void) const = 0;
    virtual ivolume_control & get_volume_control(void) = 0;
    virtual ivolume_control const & get_volume_control(void) const = 0;
};

struct imatrix_mixer
{
    using volume_handler_t = std::function<void(imatrix_volume &)>;
    /* Sends the current matrix input and the previous enumeration number. */
    using input_handler_t = std::function<void(imatrix_input &, unsigned)>;
    virtual ~imatrix_mixer(void) {};
    virtual unsigned get_input_count(void) const = 0;
    virtual unsigned get_mix_count(void) const = 0;
    virtual imatrix_volume & get_matrix_volume(unsigned input_num, unsigned mix) = 0;
    virtual imatrix_volume const & get_matrix_volume(unsigned input_num, unsigned mix) const = 0;
    virtual imatrix_input & get_matrix_input(unsigned) = 0;
    virtual imatrix_input const & get_matrix_input(unsigned) const = 0;
    virtual void add_volume_listener(volume_handler_t const &) = 0;
    virtual void add_input_listener(input_handler_t const &) = 0;
};

} /* namespace scarlett */
