#pragma once

#include "exception"

namespace scarlett
{

using ienum_ptr = std::shared_ptr<struct ienum_control>;
using ienum_const_ptr = std::shared_ptr<struct ienum_control const>;

struct ienum_control
{
    /* Returns the changed enum control and the previous selection. */
    using enum_handler_t = std::function<void(ienum_control &, unsigned)>;
    virtual ~ienum_control(void) {};
    virtual std::vector<std::string> get_enum_choices(void) const = 0;
    virtual std::string get_name(void) const = 0;
    virtual unsigned get_selection(void) const = 0;
    virtual void set_selection(unsigned) = 0;
    /* Throws exception if the specified string is not a viable
     * enum value. */
    virtual void set_selection(std::string const &);
    virtual std::string get_str_selection(void) const;
    virtual void register_enum_changed(void const *, enum_handler_t const &) = 0;
    virtual void unregister_all(void const *) = 0;
    virtual void unregister_all(void) = 0;
};

} /* namespace scarlett */
