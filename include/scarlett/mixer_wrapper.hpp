#pragma once

#include "ivolume_control.hpp"
#include "ienum_control.hpp"
#include "imixer.hpp"
#include <mutex>

namespace scarlett
{

struct stereo_pair
{
    int num;
    std::string element0;
    std::string element1;
    /* This number is used when this does not correspond to a stereo pair. */
    static constexpr int no_pair {-1};
};

struct input_assignment
{
    std::string input;
    int vol;
};

/* The Scarlett matrix design is quite flexible, but can prove a bit cumbersome
 * to work with directly. This wrapper provides a different interface. The paradigm
 * used is the idea of mixing inputs to outputs in a flexible way. Of course, the
 * pre-existing limitations of the underlying scarlett matrix are still imposed using
 * runtime exceptions. */
struct mixer_wrapper final
{
    using output_volume_changed_handler_t = std::function<void(std::string const & output, int new_volume)>;
    using volume_changed_handler_t = std::function<void(int new_volume)>;
    /* Called when the specified input on the specified output's volume changes. */
    using mix_volume_changed_handler_t = std::function<void(std::string const & output, std::string const & input, int new_volume)>;
    /* If added is true, the specified
     * input was added to the specified output. If false, it was removed. */
    using mix_output_changed_handler_t = std::function<void(std::string const & output, std::string const & input, bool added)>;
    using stereo_pair_changed_handler_t = std::function<void(int pair, int volume, double pan)>;

    /* This value is used to signify "no output" or "no input" */
    static std::string const off_option;
    static int const no_volume;

    mixer_wrapper(imixer &);
    ~mixer_wrapper(void);

    /* Returns all the options for inputs to output mixes. */
    std::vector<std::string> get_mix_inputs(void) const;
    /* Returns all the output names including USB outputs. */
    std::vector<std::string> get_outputs(void) const;

    int get_mix_input_max_db(std::string const & output, std::string const & input) const;
    int get_mix_input_min_db(std::string const & output, std::string const & input) const;
    int get_master_volume(void) const;
    void set_master_volume(int);
    /* USB Outputs cannot have volume. If a USB Output name is passed, vo_volume is returned. */
    int get_output_volume(std::string const &) const;
    /* This can not be called on usb outputs. */
    void set_output_volume(std::string const &, int);
    /* For outputs with only one input, they don't have a mix, so this returns no_volume.
     * Volume should be set on the output itself. */
    int get_mix_volume(std::string const & output, std::string const & input) const;
    /* Throws if the current mix volume is no_volume. That is, if the input doesn't have
     * its own volume control (when there is only one input on an output), this will throw. */
    void set_mix_volume(std::string const & output, std::string const & input, int vol);

    /* Returns true if the resulting mix has volume controls for each input */
    bool add_input_to_output(std::string const & output, std::string const & input);
    /* Returns true if the resulting mix has volume controls for each input. */
    bool remove_input_from_output(std::string const & output, std::string const & input);
    /* Returns all inputs and their volumes on a given output.
     * If a volume is no_volume, that input doesn't have its own volume control.
     * (This only happens when an output has only one input.) */
    std::vector<input_assignment> get_inputs(std::string const & output) const;

    /** Creates a stereo pair out of two output channels. These are outputs like
     * master, headphones, etc. */
    int create_output_stereo_pair(std::string const &, std::string const &);
    stereo_pair get_output_stereo_pair(int) const;
    /** The pan value should be in the range [-1.0, 1.0]. */
    void set_output_stereo_pair_pan(int pair, double pan);
    void set_output_stereo_pair_volume(int pair, int vol);
    double get_output_stereo_pair_pan(int pair) const;
    int get_output_stereo_pair_volume(int pair) const;
    int get_output_stereo_pair_min_db(int pair) const;
    /* Maximum volume panned left would be left_channel=100%, right_channel=0%.
     * Maximum volume panned center would be left_channel=71%, right_channel=71%.
     * This means that, theoretically, the maximum volume could be violated if, for instance,
     * left=100%, right=100%. */
    int get_output_stereo_pair_max_db(int pair) const;

    /* There is no way formally to unregister from these listeners. To stop
     * getting called back, make sure the mixer passed in the constructor stops
     * processing events. */
    /* Called when the volume on a mix changes externally or internally.
     * The callback could be called on the thread of a caller to another function,
     * or called on a separate, system thread. */
    void add_mix_volume_listener(mix_volume_changed_handler_t const &);
    /* Called when an input is added or removed from a mix.
     * The callback could be called on the thread of a caller to another function,
     * or called on a separate, system thread. */
    void add_mix_output_listener(mix_output_changed_handler_t const &);
    /* Called when an output's volume changes. USB outputs don't have volumes.
     * The callback could be called on the thread of a caller to another function,
     * or called on a separate, system thread. */
    void add_output_volume_listener(output_volume_changed_handler_t const &);
    /* Called when the master volume changes externally or internally.
     * The callback could be called on the thread of a caller to another function,
     * or called on a separate, system thread. */
    void add_master_volume_listener(volume_changed_handler_t const &);
    void add_output_stereo_pair_changed_listener(stereo_pair_changed_handler_t const &);

private:
    using lock_t = std::lock_guard<std::mutex>;
    std::vector<mix_volume_changed_handler_t> mix_volume_changed;
    std::vector<mix_output_changed_handler_t> mix_output_changed;
    std::vector<output_volume_changed_handler_t> output_volume_changed;
    std::vector<volume_changed_handler_t> master_volume_changed;
    std::vector<stereo_pair_changed_handler_t> output_stereo_pair_changed;
    imixer & mixer;
    mutable std::mutex mutey;
    std::unordered_map<std::string, std::vector<std::string>> output_assignments;
    std::vector<stereo_pair> output_stereo_pairs;

    ienum_control & get_output(std::string const &);
    void reset_mix(unsigned);
    unsigned get_unused_mix(void) const;
    unsigned get_output_mix(std::string const &) const;
    imatrix_input const & ensure_input_available(std::string const &);
    void assign_mix_to_output(std::string const &, unsigned);
    void master_volume_handler(ivolume_control const &);
    void output_volume_handler(ivolume_control const &);
    void output_assignment_handler(ienum_control const &, unsigned);
    void output_changed_mixes(ienum_control const &, unsigned);
    void output_added_mix(ienum_control const &, unsigned);
    void output_removed_mix(ienum_control const &, unsigned);
    void output_assignment_changed(ienum_control const &, unsigned);
    void matrix_input_changed(imatrix_input const &, unsigned);
    void matrix_input_changed_impl(imatrix_input const &, ienum_control const &, unsigned);
    void matrix_volume_changed(imatrix_volume const &);
    void call_handlers(std::string const &, std::function<void(void)> const &);
    std::optional<std::reference_wrapper<ivolume_control const>> get_volume_control(std::string const &, std::string const &) const;
    std::optional<std::reference_wrapper<ivolume_control>> get_volume_control(std::string const &, std::string const &);
    void add_input_to_mix(std::string const &, unsigned);
    void set_outputs_after_volume_change(ienum_control const &, imatrix_volume const &);
    void set_initial_output_assignments(void);
    unsigned remove_input_from_output_mix(std::string const &, std::string const &);
    std::pair<ivolume_control const *, ivolume_control const *> get_volume_elements_for_output_pair(stereo_pair const &) const;
    std::pair<ivolume_control *, ivolume_control *> get_volume_elements_for_output_pair(stereo_pair const &);
    std::vector<stereo_pair>::const_iterator get_output_stereo_pair(std::string const & out0, std::string const & out1) const;
    double get_output_stereo_pair_pan_rad(int) const;
    double get_output_stereo_pair_raw_volume(int) const;
};

} /*namespace scarlett*/
