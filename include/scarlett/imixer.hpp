#pragma once

#include <vector>
#include "ivolume_control.hpp"
#include "ienum_control.hpp"
#include "imatrix_mixer.hpp"
#include "idevice.hpp"

namespace scarlett
{

using imixer_ptr = std::unique_ptr<struct imixer>;
struct imixer
{
    using selection_handler_t = std::function<void(ienum_control &, unsigned old_selection)>;
    using volume_handler_t = std::function<void(ivolume_control &)>;
    using output_assignment_handler_t = std::function<void(ienum_control &, unsigned old_selection)>;
    using output_volume_handler_t = std::function<void(ivolume_control &)>;
    virtual ~imixer(void) = default;
    /**
     * The outputs that are sent to the USB host.
     */
    virtual unsigned get_usb_output_count(void) const = 0;
    virtual ienum_control & get_usb_output(unsigned) = 0;
    virtual ienum_control const & get_usb_output(unsigned) const = 0;
    /**
     * The outputs for monitoring by loudspeaker or headset, and other outputs.
     */
    virtual unsigned get_output_count(void) const = 0;
    virtual ienum_control & get_output_assignment(unsigned) = 0;
    virtual ienum_control const & get_output_assignment(unsigned) const = 0;
    virtual ivolume_control & get_output_volume(unsigned) = 0;
    virtual ivolume_control const & get_output_volume(unsigned) const = 0;
    /**
     * This is used to iterate over all the output assignments, "plain" outputs first.
     */
    virtual unsigned get_all_outputs_count(void) const = 0;
    virtual ienum_control & get_from_all_outputs(unsigned) = 0;
    virtual ienum_control const & get_from_all_outputs(unsigned) const = 0;
    virtual ivolume_control & get_master_volume(void) = 0;
    virtual ivolume_control const & get_master_volume(void) const = 0;
    virtual imatrix_mixer & get_matrix(void) = 0;
    virtual imatrix_mixer const & get_matrix(void) const = 0;

    /**
     * This asks the mixer to unregister from all his callbacks so that no more
     * will be issued. Better would be to have an analogy to add_usb_output_assignment_listener (etc)
     * to remove the listener.
     */
    virtual void unregister_all(void) = 0;

    virtual void add_usb_output_assignment_listener(selection_handler_t const &) = 0;
    virtual void add_output_assignment_listener(output_assignment_handler_t const &) = 0;
    virtual void add_output_volume_listener(output_volume_handler_t const &) = 0;
    virtual void add_master_volume_listener(volume_handler_t const &) = 0;
    virtual void add_settings_listener(selection_handler_t const &) = 0;
};

imixer_ptr get_mixer(idevice_ptr const &);

} /* namespace scarlett */
