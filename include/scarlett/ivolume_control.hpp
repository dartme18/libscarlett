#pragma once

#include <vector>
#include <functional>

namespace scarlett
{

using ivolume_ptr = std::shared_ptr<struct ivolume_control>;
using ivolume_const_ptr = std::shared_ptr<struct ivolume_control const>;

struct ivolume_control
{
    using volume_handler_t = std::function<void(ivolume_control &)>;
    virtual ~ivolume_control(void) {};
    virtual long get_max_db(void) const = 0;
    virtual long get_min_db(void) const = 0;
    virtual void set_db(long) = 0;
    virtual long get_db(void) const = 0;
    virtual std::string get_name(void) const = 0;
    /* The token used to unregister. */
    virtual void register_volume_changed(void const * token, volume_handler_t const &) = 0;
    /* The token originally passed to the register function. */
    virtual void unregister_all(void const * token) = 0;
    virtual void unregister_all(void) = 0;
    virtual long minimum_step_size(void) const = 0;
};

} /* namespace scarlett */
